Eine interaktive Karte von Stolpersteinen in Heidelberg; derzeit
installiert auf https://www.tfiu.de/steine.


Lizenz
======

Soweit nicht andere Rechte angegeben sind, verteilen wir das Material
nach CC-0.  Fürs Javascript vgl. leaflet/LICENSE.  Bei den Bildern ist
nach wie vor anzunehmen, dass Dritte Rechte daran haben – wer seine_ihre
Rechte verletzt sieht, möge sich an uns wenden: das ist keine Absicht,
wir werden versuchen, den Rechteverstoß zu beseitigen.


Kontakt
=======

Für die Karte und die Aufarbeitung des Materials hier: siehe
http://stolpersteine-heidelberg.de/impressum.html


Für die Stolpersteine als solche: http://stolpersteine-heidelberg.de/kontakt.html


Technisches
===========

Stolperstein-Seite
------------------

Das Repo enthält auch die HTML-Quellen für die Seite der
Stolperstein-Ini, nicht aber die dazugehörigen Binärdaten (PDFs,
Bilder; diese sind zur Erhaltung bestehender Links auf dem Zielserver in
einem festen Unterverzeichnis mediapool/x/y/z gespeichert; technische
Notiz: Markus hat eine lokale Kopie davon, die per ``unison-gtk
stolper-media`` synchronisiert wird).

Die Ini-Webseiten stehen unter web/, und zwar in HTML; das HTML darin
wird dann von einem kleinen python-Programm (bin/render-page.py) mit
res/main.template und dem Menu in res/menu.inc zu den fertigen Seiten
verbacken.  Zusammengehalten wird das tatsächlich durch res/menu.inc;
weitere Seiten müssen (mehr oder minder; render-page halt aus, wenn eine
Seite dort nicht erwähnt wird, aber es wird etwas komisch aussehen) dort
eingehängt werden.


Kurzbios
--------

Weil die gemeinsamen Darstellungen der Stolperstein-Ini für die
einzelnen Personen nicht gut verwenden können, haben wir eigene
Kurzbiografien im Unterverzeichnis bio.  Die sollten eine gemeinsame
Struktur haben::

  Vorname Nachname
  ================

  .. container:: dates

    -REPLACE-WITH-DATES-

  .. figure:: bild.jpeg

     Caption fürs Bild

  Haupt-Text

  `Mehr zu XY <https://stolpersteine-heidelberg.de/mediapool/pdf/...>`_

Nüztliche Link-Defs::

  .. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
  .. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
  .. _Noé: http://de.wikipedia.org/wiki/No%C3%A9%20%28Haute-Garonne%29
  .. _Récébédou: http://de.wikipedia.org/wiki/Portet-sur-Garonne
  .. _Wiederherstellung des Berufsbeamtentums: http://de.wikipedia.org/wiki/Gesetz%20zur%20Wiederherstellung%20des%20Berufsbeamtentums
  .. _Hermann Maas: http://de.wikipedia.org/wiki/Hermann%20Maas%20%28Theologe%29
  .. _Polenaktion: http://de.wikipedia.org/wiki/Polenaktion
  .. _Nürnberger Gesetze: https://de.wikipedia.org/wiki/N%C3%BCrnberger_Gesetze
  .. _Giovannini, Rink und Moraw: https://www.wunderhorn.de/?buecher=erinnern-bewahren-gedenken
  .. _Judenhaus: http://de.wikipedia.org/wiki/Judenhaus
  .. _Grafeneck: http://de.wikipedia.org/wiki/T%C3%B6tungsanstalt%20Schloss%20Grafeneck
  .. _Aktion T4: https://de.wikipedia.org/wiki/Aktion_T4
  .. _Konzentrationslager Dachau: http://de.wikipedia.org/wiki/KZ%20Dachau
  .. _Pogromnacht: http://de.wikipedia.org/wiki/Novemberpogrom%201938
  .. _Vichy: http://de.wikipedia.org/wiki/Vichy-Regime
  .. _Theresienstadt: http://de.wikipedia.org/wiki/KZ%20Theresienstadt
  .. _Hadamar: https://de.wikipedia.org/wiki/T%C3%B6tungsanstalt_Hadamar
  .. _Westerbork: http://de.wikipedia.org/wiki/Durchgangslager%20Westerbork
  .. _Nürnberger Prozesse: https://de.wikipedia.org/wiki/N%C3%BCrnberger_Prozesse
  .. _Gesetz über die Änderung von Familiennamen und Vornamen: https://de.wikipedia.org/wiki/Namens%C3%A4nderungsverordnung
  .. _Kindertransporte: https://de.wikipedia.org/wiki/Kindertransport
  .. _Gesetz zur Verhütung erbkranken Nachwuchses: https://de.wikipedia.org/wiki/Gesetz%20zur%20Verh%C3%BCtung%20erbkranken%20Nachwuchses
  .. _Carl Schneider: https://de.wikipedia.org/wiki/Carl_Schneider_(Mediziner)
  .. _Erbgesundheitsgericht: https://de.wikipedia.org/wiki/Erbgesundheitsgericht

Wenn es wirklich mal ein seitenfüllendes Bild braucht::

  .. figure:: fulltext.png
    :figclass: full


Stein-Fotos
-----------

Wir sammeln Fotos der Steine; das sollen jpegs mit 600x600 Pixeln sein,
wenns geht deprojiziert.  Die Fotos liegen in stein-foto, die Dateinamen
sollten den Biographie-Namen entsprechen -- aber das ist nur
Bequemlichkeit, die Zuordnung erfolgt in Spalte 7.


Koordinaten
-----------

Wir erfassen die Koordinaten der Stolpersteine per GPS, setzen sie aber
in der openstreetmap eher so, dass sie anhand der Grundrisse von
Häusern, Straßenlaternen o.ä. gefunden werden können.

Stolpersteine haben historic=memorial, memorial=stolperstein,
name=einzelner Name.  Wenn mehrere Stolpersteine eng nebeneinander
liegen, versucht, in etwa die relative Lage der Steine nachzuvollziehen.
Zwar kann gegenwärtig eigentlich nichts so weit reinzoomen, aber wer
weiß, was in Zukunft alles mit den Daten gemacht wird.

Die Koordinaten können einfach aus josm in steine.tsv übernommen werden
(Edit/Copy Coordinates).


Mapproxy
--------

Aus Datenschutzgründen, und im die OSM-Ressourcen zu schonen, nehmen wir
die Tiles aus einem großzügig konfigurierten Cache, mapproxy.  Die
Konfig ist auf sosa in /srv/mapproxy/osmproxy/mapproxy.yaml (TODO:
config hier ins VCS tun, wenn das alles funktioniert).

Der Mapproxy läuft im gunicorn als WSGI-container hinter dem nginx als
reverse proxy.

Das Ding startet dann auf sosa manuell in /srv/mapproxy/osmproxy mit::

  gunicorn -b 127.0.0.1:9080 server:application

Wenn eh ein Apache läuft, ist es einfacher, so etwas zu machen::

  WSGIScriptAlias /tiles /srv/mapproxy/osmproxy/server.py
  <Directory /srv/mapproxy/osmproxy>
    Require all granted
  </Directory>

Die Tiles kommen dann auf ``tms/1.0.0/osm/{z}/{x}/{y}.png`` raus.

Dazu gehören aus unserem tilecache-Subdir:

* mapproxy.yaml und server.py, die nach /srv/mapproxy/osmproxy müssen
  (dabei entstand server.py durch ``mapproxy-util create -t wsgi-app -f
  mapproxy.yaml server.py``).
* mapproxy als initscript (wo der Kram nicht aus einem Apache raus läuft)


Daraus wird das server.py, das es oben braucht, durch::

  mapproxy-util create -t wsgi-app -f mapproxy.yaml server.py

Wichtig dabei: /srv/mapproxy/osmproxy/cache_data muss www_data gehören.


Um die Tiles direkt von der OSM zu ziehen,

::

  URL: https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png

in die STEIN_MAP in make-page.py3 schreiben und zoomOffset und tms
auskommentieren.


Leaflet
-------

Technische Basis is Leaflet.js, https://leafletjs.com/. weil das
schmerzhaft zu bauen ist (npm), ist eine stabile Version davon
(von https://leafletjs.com/download.html) lokal hier drin, und make
build kopiert die nötigen Ressourcen von dort nach build.

Wir haben als Plugin noch leaflet-geolet, damit draußen Geolokation
geht.  Ich habe einfach https://github.com/rhlt/leaflet-geolet geclont,
nachgesehen, ob das JS plausibel aussieht, und das dann hier
reingepflanzt.


Geschichte
----------

Bootstrap der steine.tsv von
http://stolpersteine-heidelberg.de/biographie-und-orte.html
