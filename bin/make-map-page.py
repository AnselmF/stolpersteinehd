# steine.tsv in GeoJSON+HTML+Javascript ins Verzeichnis sys.argv[1] umsetzen
# Neben der Karte wird auch noch HTML für biographien-und-orte.html
# auf der statischen Seite erzeugt.

import json
import os
import re
import sys

from bs4 import BeautifulSoup


HTML_TEMPLATE = """
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Stolpersteine in Heidelberg</title>
	<meta http-equiv="content-type" content="text/xhtml;charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="res/leaflet.css"/>
	<script src="res/leaflet.js"></script>
	<script src="res/geolet.js"></script>
	<link rel="stylesheet" href="res/MarkerCluster.css"/>
	<link rel="stylesheet" href="res/MarkerCluster.Default.css"/>
	<script src="res/leaflet.markercluster.js"></script>
	<style type="text/css">
		#karte {
			width: 100%;
			height: 600px;
			background-color: #ccc;
			color: black;
			flex-grow: 1;
			z-index: 5;
		}

		a {
			color: #aaf;
		}

		.framing {
			width: 100%;
			background-color: black;
			padding: 4pt;
			transition:all .2s ease-in-out;
		}

		#head-area {
			display: flex;
			flex-direction: row;
			justify-content: space-between;
			overflow: visible;
		}

		#footer {
			font-size: 8pt;
		}

		#footer p {
			margin: 0;
		}

		#burger {
			position: fixed;
			z-index: 100;
			background-color: black;
			color: white;
			padding: 3pt;
			right: 5pt;
			top: 5pt;
			font-size: 14pt;
			font-weight: bold;
			margin: 0;
			border: none;
		}

		h1 {
			display: inline-block;
			font-size: 14pt;
			margin: 3pt;
		}

		html {
			background-color: black;
			color: #eee;
			height: 100%;
		}

		body {
			display: flex;
			flex-direction: column;
			height: 100%;
			margin: 0;
			overflow: hidden;
		}

		.leaflet-popup .steinfoto {
			width: 120pt;
		}

		.leaflet-popup {
			width: 140pt;
		}
	</style>

	<script type="text/javascript">//<![CDATA[
		var STEINE = ###geojson###;
		var STEIN_MAP;
		var DEFAULT_COORDS = [13, 49.40, 8.68];

		function addPointBehaviour(feature, layer) {
			var im = `${feature.properties.steinimg}`;
			var name = feature.properties.name;
			var adr = feature.properties.address;
			var sb = feature.properties.shortbio;
			var content = `${name}<br/>${sb}`;
			var curloc = `18/${feature.geometry.coordinates[1]}/${feature.geometry.coordinates[0]}`;

			if (im) {
				 content = `<img class="steinfoto" src='${im}'`
				 	+`alt="${name}, ${sb}"/>`
			}

			layer.bindPopup(`${adr}<br/>${content}<br/>`
				+`<a href='${feature.properties.mediaurl}'>[Biographisches]</a>`);
		}

		function updateLocation(ev) {
			// callback für moveend auf der Karte: die Browser-URL
			// aktualisieren, so dass die Leute sowas bookmarken können.
			var p = STEIN_MAP.getCenter();
			window.location.hash = `#${STEIN_MAP.getZoom()}/${p.lat}/${p.lng}`;
		}

		function makeMarker(feature, latlng) {
			// gibt einen leaflet-Marker für einen Stolperstein zurück
			// Unser kram ist in feature.properties.
			return L.marker(latlng, {
				opacity: 0.8,
				riseOnHover: true,
				title: feature.properties.name,
			});
		}

		function setupMap() {
			// setzt die Karte auf; dabei werten wir ein URL-Fragment
			// der Form zoom/lat/long aus.
			var coords = window.location.hash.slice(1).split("/").map(
				parseFloat);
			if (! coords[0] || ! coords[1] || ! coords[2]) {
				coords = DEFAULT_COORDS;
			}

			STEIN_MAP = L.map("karte").setView([coords[1], coords[2]], coords[0]);
			L.tileLayer(
				'/tiles/tms/1.0.0/osm/{z}/{x}/{y}.png', {
//				'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				attribution: 'Kartendaten: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
				maxZoom: 19,
				zoomOffset: -1,
				tms: true,
			}).addTo(STEIN_MAP);

			var clusterGroup = L.markerClusterGroup();
			var steinLayer = L.geoJSON(STEINE, {
				pointToLayer: makeMarker,
				onEachFeature: addPointBehaviour});
			clusterGroup.addLayer(steinLayer);
			STEIN_MAP.addLayer(clusterGroup);

			L.geolet({
				position: 'bottomleft'
			}).addTo(STEIN_MAP);

			STEIN_MAP.addEventListener("moveend", updateLocation);
		};

		window.addEventListener("load", setupMap);

	//]]></script>


<style type="text/css">
.grouper {
	display: inline-block;
	position: relative;
	flex-grow: 1;
	margin-left: 2ex;
	margin-right:1ex;
}

.spacer {
	padding: 0 15pt 0 15pt;
}

#steinfinder {
	position: relative;
	display: inline-block;
	width: 100%;
}

#matches {
	position: absolute;
	background: white;
	width: 20rem;
	height: auto;
	max-height: 30rem;
	scrollbars: auto;
	border: 1px solid black;
	display: none;
	top: 1rem;
	left: 0pt;
	overflow: scroll;
	list-style-type: none;
	z-index: 100000;
	color: black;
	padding: 0pt;
}

#matches li {
	cursor: default;
	padding-left: 0.3rem;
}

#matches li:hover {
	background-color: #bbf;
}

h1 a {
	color: white;
	text-decoration: none;
}
</style>


<script type="text/javascript">// <![CDATA[
function setupLocator() {
	// richtet das steinfinder-Feld als combo-Box zum Finden nach
	// Namen im (globalen) GeoJSON STEINE ein; es braucht auch noch
	// ein matches-div.  Dazu gibts oben auch noch CSS.
	var nameInput = document.getElementById("steinfinder");
	var matchesBox = document.getElementById("matches");

	function selectStein(ev) {
		// der callback fürs Klicken in die matchesBox.
		matchesBox.style["display"] = "none";
		var stein = STEINE.features[
			parseInt(ev.currentTarget.getAttribute("data"))];
		nameInput.value = stein.properties.name;
		STEIN_MAP.setView(L.latLng(
			parseFloat(stein.geometry.coordinates[1]),
			parseFloat(stein.geometry.coordinates[0])), 19);
	}

	function updateMatches(ev) {
		// der Callback für Änderungen in nameInput
		var inputSoFar = nameInput.value.toLowerCase();
		var matches = [];

		if (inputSoFar) {
			for (var index=0; index<STEINE.features.length; index++) {
				var stein = STEINE.features[index];
				if (stein.properties.name.toLowerCase().indexOf(inputSoFar)!=-1) {
					matches.push(new Array(stein.properties.name, index));
				}
				if (matches.length==100) break;
			}
			matches.sort();
		}

		if (matches.length==0) {
			matchesBox.style["display"] = "none";
		} else {
			matchesBox.style["display"] = "block";
		}

		matchesBox.innerHTML = '';
		for (var matchedStein of matches) {
			var newEl = document.createElement("li");
			newEl.setAttribute("data", matchedStein[1]);
			newEl.innerHTML = matchedStein[0];
			newEl.addEventListener("click", selectStein);
			matchesBox.append(newEl);
		}
	}

	nameInput.addEventListener("keyup", updateMatches);
}

function hideFrame() {
	document.querySelector("#head-area").style.height = "3px";
	document.querySelector("#footer").style.height = "3px";
	document.querySelector("#burger").framestate = "closed";
}

function showFrame() {
	document.querySelector("#head-area").style.height = "auto";
	document.querySelector("#footer").style.height = "auto";
	document.querySelector("#burger").framestate = "opened";
}

function toggleFrame(ev) {
	if (ev.target.framestate=="closed") {
		showFrame();
	} else {
		hideFrame();
	}
}

window.addEventListener('load', function() {
	setupLocator();
	if (document.body.clientHeight<700) {
		window.setTimeout(function() {
			document.querySelector("#burger").addEventListener(
				"click", toggleFrame);
			hideFrame();
			}, 3000);
	} else {
			document.querySelector("#burger").style.display = 'none';
	}
}); //]]></script>
</head>
<body>
	<div id="head-area" class="framing">
	<h1><a href="/">Stolpersteine in Heidelberg</a></h1>

	<div class="grouper">
		<input type="text" id="steinfinder" placeholder="Name">
		<ul id="matches"/></input>
	</div>

	<div class="spacer"> </div>
	</div>

	<div id="karte">
		<p>Hier sollten gleich Karten-Tiles kommen.  Wenn nicht, hast du
		wahrscheinlich Javascript verboten.
		Das ist zwar lobenswert, aber Karten gehen ohne halt
		nicht so besonders gut.  Wir ziehen aber immerhin kein Javascript
		von woanders her nach, und die Karten-Bilder kommen entweder
		direkt von uns (wenn sie im Cache sind) oder von der Openstreetmap,
		und auch dann kommen die Requests von uns und nicht von euch.</p>
	</div>
	<div id="footer" class="framing">
		<p><strong>Quelle:</strong>
			<a href="http://stolpersteine-heidelberg.de">Stolperstein-Initiative</a>.
		<strong>Datenschutz:</strong> Wir loggen nicht, und die Karte sollte
		nur mit unserem Server reden.
		<a href="https://codeberg.org/AnselmF/stolpersteinehd">Kontakt/Lizenz/Quelltext</a></p>
	</div>
	<button id="burger">☰</button>
</body>
</html>
"""


def fill_template(template, fillers):
	"""ein ganz schlichter templater: ersetze ###key### aus fillers.
	"""
	return re.sub("###([^#]+)###",
		lambda mat: fillers[mat.group(1)],
		template)


def parse_input(src_name):
	"""iteriert über die Stolperstein-Metadaten als dicts zurück.

	Wir lesen von src_name (hier: steine.tsv).
	"""
	keys = ["lat", "long", "name", "address", "mediaurl",
		"shortbio", "steinimg"]
	with open(src_name) as f:
		for ln in f:
			if ln.startswith("#"):
				continue
			res = dict(zip(keys, ln.strip().split("\t")))

			if "shortbio" in res:
				res["shortbio"] = "{}".format(res["shortbio"])
			else:
				res["shortbio"] = ""
			
			yield res


def make_stein_geo(stein):
	"""gibt ein GeoJSON-Feature für ein stein-Dictionary zurück.
	"""
	if stein.get("steinimg"):
		steinimg = "stein-foto/"+stein["steinimg"]+".jpeg"
	else:
		steinimg = ""

	return {
		"type": "Feature",
		"geometry": {
			"type": "Point",
			"coordinates": [stein["long"], stein["lat"]],},
		"properties": {
			"name": stein["name"],
			"address": stein["address"],
			"shortbio": stein["shortbio"],
			"mediaurl": stein["mediaurl"],
			"steinimg": steinimg,
			"sym": None,},
	}


def get_geojson(steine):
	"""gibt die Metadaten in steine (cf. parse_input) nach als geojson-String
	zurück
	"""
	geo_struct = {
		"type": "FeatureCollection",
		"features": [
			make_stein_geo(s) for s in steine],}
	return json.dumps(geo_struct, indent=True)


def guess_last_name(n):
	# Wir probieren mit "letztes element vor einem eventuellen Komma"
	# durchzukommen.  Mal sehen, wie oft das nicht funktioniert.
	n = re.sub(r"\([^)]+\)", "", n)
	return n.split(",")[0].strip().split()[-1]


def make_index_entry(rec):
	"""gibt html für einen Index-Eintrag einer stein-zeile rec zurück.
	"""
	geourl = "karte.html#19/{lat}/{long}".format(**rec)
	name, address = rec["name"], rec["address"]

	# Wir könnten hier gleich zu den Karten-Biographien linken;
	# historisch aber gingen die Links im Index auf die
	# Broschüren-Ausschnitte, und das lasse ich auch erstmal so.
	# Aber: ich muss die aus den Biographie-Seiten klauben; das ist
	# erstmal ein Hack, wenn das so bleiben soll, sollte das irgendwann
	# anders werden.
	if rec["mediaurl"].endswith(".pdf"):
		# es gibt noch keine gesonderte bio
		biourl = rec["mediaurl"]
	else:
		# fummele die PDF-URL aus der gerenderten Bio raus
		try:
			with open(rec["mediaurl"], encoding="utf-8") as f:
				soup = BeautifulSoup(f.read(), "xml")
		except IOError:
			sys.stderr.write(f"Keine Biographie, skippe: {rec}.\n")
			return ""

		upstream_ref = soup.find_all("a", {"class": "reference external"})[-1]
		try:
			biourl = upstream_ref["href"].split("stolpersteine-heidelberg.de/")[1]
		except IndexError:
			return "MISSING"

	return (
		'<li><a class="biolink" href="{biourl}">{name}</a>'
		' <a class="geolink" href="{geourl}">{address}</a></li>\n'.format(
			**locals()))


def make_bio_orte(steine):
	recs = [(guess_last_name(rec["name"]), rec)
		for rec in steine
		# keine nicht-Heidelberg-Steine hier
		if rec["address"][0]!='(']

	by_initial = {}
	for last_name, rec in recs:
		by_initial.setdefault(last_name[0], []).append((last_name, rec))
	for rec_list in by_initial.values():
		rec_list.sort(key=lambda p: (p[0], p[1]["name"]))

	result = [
		'<!-- NICHT EDITIEREN! Ist aus steine.tsv erzeugt -->',
		'<h1>Biographien und Orte</h1>',]

	for initial in sorted(by_initial):
		result.extend([
			f'<h2>{initial}...</h2>',
			f'<ul class="person-index">\n'])

		for _, rec in by_initial[initial]:
			result.append(make_index_entry(rec))

		result.append('</ul>\n')
	
	return "\n".join(result)


def main():
	if len(sys.argv)!=2:
		sys.exit("Usage: {} <dest-dir>".format(sys.argv[0]))

	dest_dir = sys.argv[1]
	os.makedirs(dest_dir, exist_ok=True)

	steine = list(parse_input("steine.tsv"))
	geojson = get_geojson(steine)
	with open(
			os.path.join(dest_dir, "karte.html"),
			"w", encoding="utf-8") as f:
		f.write(fill_template(HTML_TEMPLATE, locals()))

	with open("web/biographien-und-orte.html",
			"w", encoding="utf-8") as f:
		f.write(make_bio_orte(steine))


if __name__=="__main__":
	main()
