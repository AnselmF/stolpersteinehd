#!/usr/bin/env python
# This script updates the stolperstein index.html with events from sofo-hd
# (tagged "stolpersteine").  This is designed to run on sosa about nightly.
#
# It assumes it is running in the directory with index.html and needs the
# magic comments <!-- sofoterm start --> and <!-- sofoterm end -->.
#
# Dependency: python3-bs4, python3-requests

import re

import requests
from bs4 import BeautifulSoup

SOFO_URL = ("https://sofo-hd.de/list?nDays=0&tag=stolperstein"
	"&title=&style=&plain=true&useFullURLs=true&breakout=true")

START_MARKER = "<!-- sofoterm start -->"
END_MARKER = "<!-- sofoterm end -->"


def main():
	try:
		soup = BeautifulSoup(requests.get(SOFO_URL).text, "lxml").body
	except requests.ConnectionError:
		return

	formatted = "\n".join(str(c) for c in soup.children)

	with open("index.html", encoding="utf-8") as f:
		content = f.read()

	if not soup.find("h3"):
		# no events, skip section
		formatted = ""
	else:
		formatted = f"""
			<div class="sofo-termine">
			<h1>Termine</h1>
			{formatted}
			</div>"""

	content = re.sub(
		f"(?s){START_MARKER}.*{END_MARKER}",
		f"{START_MARKER}\n{formatted}\n{END_MARKER}",
		content)
	
	with open("index.html", "w", encoding="utf-8") as f:
		f.write(content)


if __name__=="__main__":
	main()

