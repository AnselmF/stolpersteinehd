#!/usr/bin/env python3
# extract menu links from the botched original pages
# (not necessary in operations, just a quick hack for Markus during
# migration)
import pathlib
import os
import re
import sys
from bs4 import BeautifulSoup

tree = BeautifulSoup(
	open(os.path.join("/var/www/stolpersteine", sys.argv[1])), "lxml")

if False:
	# extract 2nd level links
	for el in tree.find_all("a"):
		anchor = el.find("img")
		if anchor and anchor.get("title"):
			print('  <li><a href="{}">{}</a></li>'.format(
				el.get("href"),
				anchor.get("title")))

else:
	dest_dir = pathlib.Path("web")
	# extract html/pdf mappings
	for el in tree.find_all("a"):
		if el.get("onclick"):
			mat = re.search(r"window\.open\('([^']+)'", el["onclick"])
			if mat:
				dest_name = dest_dir / (el["href"]+".redirect")
				if not dest_name.exists():
					with dest_name.open("w") as f:
						f.write(re.sub(
								"https?://stolpersteine-heidelberg.de/",
								"",
								mat.group(1)))

