"""
Build the main stolperstein web pages, based on a simple template
and a menu.

This is intended to be run in the parent of the res directory.
"""

import copy
import sys
import pathlib
import os

from bs4 import BeautifulSoup

with open("res/main.template", "r", encoding="utf-8") as f:
	TEMPLATE = f.read()
with open("res/redirect.template", "r", encoding="utf-8") as f:
	REDIRECT_TEMPLATE = f.read()
with open("res/menu.inc", encoding="utf-8") as f:
	MENU = BeautifulSoup(f, "lxml")


def get_menu_for_file_name(f_name):
	menu = copy.copy(MENU)
	for a in menu.find_all("a"):
		if a.get("href")==f_name:
			a.parent["class"] = "thispage"
			if "navmenu-inner" in a.parent.parent["class"]:
				# we're in an inner menu and need to open the outer one, too
				a.parent.parent.parent["class"] = "thispage"
	return menu


def format_one(src_file, dest_dir):
	with open(src_file, encoding="utf-8") as f:
		tree = BeautifulSoup(f, "lxml")

	title_el = tree.find("h1")
	# fillers must be UTF-8 bytestrings
	fillers = {
		"title": (title_el.text
			if title_el
			else "Stolpersteine in Heidelberg"),
		"body": str(tree),
		"menu": str(get_menu_for_file_name(str(src_file.name)))}
	formatted = TEMPLATE.format(**fillers)

	with open(dest_dir / src_file.name, "w", encoding="utf-8") as f:
		f.write(formatted)


def make_one_redirect(src_file, dest_dir):
	html_name = src_file.name[:-9]
	with open(src_file, encoding="utf-8") as f:
		fillers = {
			"dest_addr": f.read().strip(),
			"menu": str(get_menu_for_file_name(html_name))}

	formatted = REDIRECT_TEMPLATE.format(**fillers)
	with open(dest_dir / html_name, "w", encoding="utf-8") as f:
		f.write(formatted)


def build_site(src, dest):
	dest.mkdir(exist_ok=True)
	for src_file in src.glob("*.html"):
		format_one(src_file, dest)

	for src_file in src.glob("*.redirect"):
		make_one_redirect(src_file, dest)


if __name__=="__main__":
	if False:  # for debugging
		format_one(
			pathlib.Path("web/verfolgung-in-hd.html"),
			pathlib.Path(sys.argv[1]))
	else:
		build_site(pathlib.Path("web"), pathlib.Path(sys.argv[1]))
