#!/usr/bin/env python
# RE-clean some of the worse offenses against HTML style from original
# stolperstein input files.  Not needed in normal operations, just a quick
# hack for Markus.

import os
import re
import sys
from html import entities

def translate_entity(mat):
	ref = mat.group(1)
	if ref.startswith("#"):
		return chr(int(ref[1:-1]))
	else:
		return entities.html5[mat.group(1)]


with open(os.path.join("/var/www/stolpersteine", sys.argv[1])) as f:
	content = f.read()

content = re.sub("</?(font|span|b)[^>]*>", "", content)
content = re.sub("<p [^>]*>", "<p>", content)
content = re.sub("&([^;]+;)",
	translate_entity,
	content)

content = re.sub("(?s)<p>(.*?)</p>",
	lambda mat: "\n<p>{}</p>\n".format(re.sub(r"\s+", " ", mat.group(1))),
	content)

with open(os.path.join("web", sys.argv[1]), "w") as f:
	f.write(content)
