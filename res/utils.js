/* make menu items clickable full-width */

function click_embedded_link(ev) {
	let a_el = ev.target.querySelector("a");
	if (a_el) {
		window.location = a_el.href;
	}
}

function make_menu_clickable() {
	for (let li of document.querySelectorAll(".navmenu li")) {
		li.addEventListener('click', click_embedded_link)
	}
}

document.addEventListener('DOMContentLoaded', function(ev) {
	make_menu_clickable();
});
