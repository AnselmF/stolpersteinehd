<ul class="navmenu">
<li><a href="index.html">Startseite</a></li>
<li><a href="karte.html">Karte</a></li>
<li><a href="verlegte-steine-2010.html">Verlegte Steine 2010</a>
	<ul class="navmenu-inner">
  <li><a href="heinrich-fehrentz.html">Heinrich Fehrentz</a></li>
  <li><a href="ludwig-brummer.html">Ludwig Brummer</a></li>
  <li><a href="familie-durlacher.html">Familie Durlacher</a></li>
  <li><a href="familie-blum.html">Familie Blum</a></li>
  <li><a href="leontine-goldschmidt.html">Leontine Goldschmidt</a></li>
  <li><a href="familie-liebhold.html">Familie Liebhold</a></li>
	</ul></li>

<li><a href="verlegte-steine-2011.html">Verlegte Steine 2011</a><
	<ul class="navmenu-inner">
  <li><a href="m-l-neu.html">M. & L. Neu</a></li>
  <li><a href="j-schneider-a-bock.html">J.Schneider & A.Bock</a></li>
  <li><a href="gustav-bopp.html">Gustav Bopp</a></li>
  <li><a href="fam-kaufmann-flor.html">Fam. Kaufmann, Flor</a></li>
  <li><a href="kaethe-alfred-seitz.html">Käthe & Alfred Seitz</a></li>
  <li><a href="familie-baer.html">Familie Baer</a></li>
  <li><a href="familie-freund.html">Familie Freund</a></li>
  <li><a href="hermann-boening.html">Hermann Böning</a></li>
	</ul></li>

<li><a href="verlegte-steine-2012.html">Verlegte Steine 2012</a>
	<ul class="navmenu-inner">
  <li><a href="maja-bitsch.html">Maja Bitsch</a></li>
  <li><a href="albert-fritz.html">Albert Fritz</a></li>
  <li><a href="bruno-oppenheimer.html">Bruno Oppenheimer</a></li>
  <li><a href="fam-hochherr-weil.html">Fam. Hochherr Weil</a></li>
  <li><a href="sal-paula-deutsch.html">Sal. Paula Deutsch</a></li>
  <li><a href="fam-simon-hochherr.html">Fam. Simon Hochherr</a></li>
  <li><a href="julius-rinklin.html">Julius Rinklin</a></li>
	</ul></li>

<li><a href="verlegte-steine-2013.html">Verlegte Steine 2013</a>

	<ul class="navmenu-inner">
  <li><a href="sowj-zangsarbeiter.html">Sowj. Zangsarbeiter</a></li>
  <li><a href="eheleute-oppenheimer.html">Eheleute Oppenheimer</a></li>
  <li><a href="familie-snopek.html">Familie Snopek</a></li>
  <li><a href="johanna-geissmar.html">Johanna Geißmar</a></li>
  <li><a href="anna-klara-hamburger.html">Anna Klara Hamburger</a></li>
  <li><a href="familie-seligmann.html">Familie Seligmann</a></li>
  <li><a href="julie-jankau.html">Julie Jankau</a></li>
  <li><a href="gutman-mombert.html">Gutman Mombert</a></li>
  <li><a href="familie-geissmar.html">Familie Geißmar</a></li>
	</ul></li>

<li><a href="verlegte-steine-2014.html">Verlegte Steine 2014</a>

	<ul class="navmenu-inner">
  <li><a href="familie-seligman.html">Familie Seligman</a></li>
  <li><a href="familie-sommer.html">Familie Sommer</a></li>
  <li><a href="familie-fisch.html">Familie Fisch</a></li>
  <li><a href="richard-max-broosch.html">Richard Max Broosch</a></li>
  <li><a href="familie-mueller.html">Familie Müller</a></li>
  <li><a href="familie-kuhn.html">Familie Kuhn</a></li>
  <li><a href="elise-dosenheimer.html">Elise Dosenheimer</a></li>
  <li><a href="familie-bettmann.html">Familie Bettmann</a></li>
  <li><a href="sussmanowitz-szekely.html">Sussmanowitz Szekely</a></li>
	</ul></li>

<li><a href="verlegte-steine-2015.html">Verlegte Steine 2015</a>

	<ul class="navmenu-inner">
  <li><a href="familie-blumberg.html">Familie Blumberg</a></li>
  <li><a href="familie-demuth.html">Familie Demuth</a></li>
  <li><a href="familie-leser.html">Familie Leser</a></li>
  <li><a href="max-rose-wertheimer.html">Max u. Rose Wertheimer</a></li>
  <li><a href="familie-wertheimer.html">Familie Wertheimer</a></li>
	</ul></li>

<li><a href="verlegte-steine-2016.html">Verlegte Steine 2016</a>

	<ul class="navmenu-inner">
  <li><a href="klara-naegele.html">Klara Nägele</a></li>
  <li><a href="isak-betti-engelberg.html">Isak Betti Engelberg</a></li>
  <li><a href="hirsch-maienthal.html">Hirsch Maienthal</a></li>
  <li><a href="max-mina-ledermann.html">Max u. Mina Ledermann</a></li>
  <li><a href="kahn.html">Eheleute Kahn</a></li>
  <li><a href="storch-ziegler-hei.html">Storch, Ziegler, Heiselbeck</a></li>
  <li><a href="himmelstern.html">Familie Himmelstern</a></li>
  <li><a href="heinr-caecilie-wahl.html">Familie Wahl</a></li>
  <li><a href="sophie-nathan-wolff.html">Sophie Nathan Wolff</a></li>
  <li><a href="mayer.html">Mayer</a></li>
  <li><a href="beer.html">Beer</a></li>
	</ul></li>

<li><a href="verlegte-steine-2017.html">Verlegte Steine 2017</a>

	<ul class="navmenu-inner">
  <li><a href="familie-bodem.html">Familie Bodem</a></li>
  <li><a href="familie-kahn.html">Familie Kahn</a></li>
  <li><a href="familie-gutmann.html">Familie Gutmann</a></li>
  <li><a href="familie-weiner.html">Familie Weiner</a></li>
  <li><a href="familie-meyer.html">Familie Meyer</a></li>
  <li><a href="doris-baum.html">Doris Baum</a></li>
  <li><a href="karoline-borchardt.html">Karoline Borchardt</a></li>
  <li><a href="dr-dora-busch.html">Dr. Dora Busch</a></li>
  <li><a href="dr-eugen-ehrmann.html">Dr. Eugen Ehrmann</a></li>
  <li><a href="dr-berta-eisenmann.html">Dr. Berta Eisenmann</a></li>
  <li><a href="margot-meyer.html">Margot Meyer</a></li>
  <li><a href="helene-preetorius.html">Helene Preetorius</a></li>
  <li><a href="familie-simon.html">Familie Simon</a></li>
	</ul></li>

<li><a href="verlegte-steine-2020.html">Verlegte Steine 2020</a>

	<ul class="navmenu-inner">
  <li><a href="familie-ehrmann.html">Familie Ehrmann</a></li>
  <li><a href="bertha-menges.html">Bertha Menges</a></li>
  <li><a href="sofie-metzger.html">Sofie Metzger</a></li>
  <li><a href="eheleute-polak.html">Eheleute Polak</a></li>
  <li><a href="familie-lammfromm.html">Familie Lammfromm</a></li>
  <li><a href="rosa-gruenbaum.html">Rosa Grünbaum</a></li>
  <li><a href="max-eisemann.html">Max Eisemann</a></li>
  <li><a href="dr-berthold-fuchs.html">Dr. Berthold Fuchs</a></li>
  <li><a href="eheleute-weinberger.html">Eheleute Weinberger</a></li>
  <li><a href="fritz-harrer.html">Fritz Harrer</a></li>
  <li><a href="anna-joerder.html">Anna Jörder</a></li>
  <li><a href="jakob-zahn.html">Jakob Zahn</a></li>
  <li><a href="familie-bornstein.html">Familie Bornstein</a></li>
  <li><a href="lucia-oestringer.html">Lucia Östringer</a></li>
  <li><a href="hubert-weidinger.html">Hubert Weidinger</a></li>
  <li><a href="paul-becker.html">Paul Becker</a></li>
  <li><a href="eheleute-v-waldberg.html">Eheleute v. Waldberg</a></li>
  <li><a href="willi-hartlieb.html">Willi Hartlieb</a></li>
  <li><a href="irene-schaefer.html">Irene Schäfer</a></li>
  <li><a href="barbara-gaertner.html">Barbara Gärtner</a></li>
  <li><a href="jakob-leonhard.html">Jakob Leonhard</a></li>
  <li><a href="ursula-haug.html">Ursula Haug</a></li>
  <li><a href="familie-linick.html">Familie Linick</a></li>
	</ul></li>

<li><a href="verlegte-steine-2021.html">Verlegte Steine 2021</a>

	<ul class="navmenu-inner">
	<li><a href="frieda-mayer.html">Frieda Mayer</a></li>
  <li><a href="familie-pagel.html">Familie Pagel</a></li>
  <li><a href="camilla-jellinek.html">Camilla Jellinek</a></li>
  <li><a href="familie-romhanyi.html">Familie Romhanyi</a></li>
  <li><a href="familie-levy.html">Familie Levy</a></li>
  <li><a href="familie-orenstein.html">Familie Orenstein</a></li>
  <li><a href="herta-dietr-flamme.html">Herta Dietr. Flamme</a></li>
  <li><a href="frieda-straus.html">Frieda Straus</a></li>
  <li><a href="familie-guhrauer.html">Familie Guhrauer</a></li>
  </ul>

<li><a href="verlegte-steine-2022.html">Verlegte Steine 2022</a>
	<ul class="navmenu-inner">
	<li><a href="familie-hachenburg.html">Familie Hachenburg</a></li>
	<li><a href="herrmann-rosenfeld.html">Herrmann Rosenfeld</a></li>
	<li><a href="frieda-fromm-reich.html">Frieda Fromm-Reich.</a></li>
	<li><a href="karl-moser.html">Karl Moser</a></li>
	<li><a href="fam-herrmann-sipper.html">Fam. Herrmann Sipper</a></li>
	<li><a href="familie-jakob-sipper.html">Familie Jakob Sipper</a></li>
	<li><a href="familie-less.html">Familie Less</a></li>
	<li><a href="familie-seidemann.html">Familie Seidemann</a></li>
	<li><a href="familie-mayer.html">Familie Mayer</a></li>
	<li><a href="familie-strauss.html">Familie Strauss</a></li>
	</ul></li>

<li><a href="verlegte-steine-2024.html">Verlegte Steine 2024</a>
	<ul class="navmenu-inner">
	<li><a href="/mediapool/pdf/2024-ackermann-tj.pdf">Theodor Ackermann</a></li>
	<li><a href="/mediapool/pdf/2024-lacher-wi.pdf">Wilhelm Lacher</a></li>
	<li><a href="/mediapool/pdf/2024-fam-deutsch.pdf" >Familie Deutsch</a></li>
	<li><a href="/mediapool/pdf/2024-fam-hess.pdf">Fam Hess</a></li>
	<li><a href="/mediapool/pdf/2024-bechtel-mi.pdf">Mina Bechtel</a></li>
	<li><a href="/mediapool/pdf/2024-fam-beer-oppenheimer.pdf"
		>Beer, Oppenheimer, Sterling</a></li>
	<li><a href="/mediapool/pdf/2024-fam-sternweiler-wertheimer.pdf"
		>Sternweiler, Wertheimer</a></li>
	<li><a href="/mediapool/pdf/2024-fam-gumbel.pdf"
		>Familie Gumbel</a></li>
	<li><a href="/mediapool/pdf/2024-e-e-reis.pdf"
		>Edwin und Elsbeth Reis</a></li>
	<li><a href="/mediapool/pdf/2024-m-w-reis.pdf"
		>Mathilde und Wilhelm Reis</a></li>
	</ul></li>

<li><a href="publikationen.html">Publikationen</a></li>
<li><a href="biographien-und-orte.html">Biographien und Orte</a></li>
<li><a href="geschichte.html">Geschichte</a>
	<ul class="navmenu-inner">
	<li><a href="verfolgung-in-hd.html">Verfolgung in HD</a></li>
	<li><a href="zwangsarbeiter.html">Zwangsarbeiter</a></li>
	<li><a href="gursdeportation-1940.html">Gursdeportation 1940</a></li>
	<li><a href="sinti-und-roma.html">Sinti und Roma</a></li>
	<li><a href="euthanasie-morde.html">Euthanasie-Morde</a></li>
	</ul></li>
<li><a href="literaturhinweise.html">Literaturhinweise</a></li>
<li><a href="steine-putzen.html">Steine putzen</a></li>
<li><a href="kontakt.html">Kontakt</a></li>
<li><a href="impressum.html">Impressum</a></li>
<li><a href="spendenkonto.html">Spendenkonto</a></li>
</ul>
