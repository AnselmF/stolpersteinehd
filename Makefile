build: build/bio bin/make-map-page.py steine.tsv leaflet web res
	python3 bin/render-web-pages.py $@
	rsync -av res/ $@/res/
	(cd $@; python3 ../bin/update-from-sofo.py)

build/bio: bio steine.tsv bin/make-map-page.py
	python3 bin/make-map-page.py $@/..
	mkdir -p $@/res
	cp -r leaflet/geolet.js leaflet/leaflet.css leaflet/leaflet.js leaflet/images/* $@/../res/
	cp markercluster/leaflet.markercluster.js markercluster/MarkerCluster.css\
	  markercluster/MarkerCluster.Default.css $@/../res/
	(cd bio; make)
	rsync -av bio/ build/bio/
	rsync -av stein-foto/ build/stein-foto/

install: build
	rsync --exclude mediapool -av build/ steine@sosa.tfiu.de:/var/www/stolpersteine/

clean:
	rm -rf build
	(cd bio; make clean)
