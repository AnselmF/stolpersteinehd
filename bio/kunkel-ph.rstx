Philipp Kunkel
==============

.. container:: dates

  -REPLACE-WITH-DATES-

Philipp Kunkel wurde am 23. August 1878 in Dossenheim geboren.  Wir
wissen nicht viel über sein Leben: Er arbeitete in den Steinbrüchen des
Ortes, war mit Barbara Kunkel verheiratet und wohnte mit ihr in der
Dossenheimer Mühltalstraße 4.

Wir wissen auch nicht, wer ihn weshalb am 9. April 1941 in die Wieslocher
„Heil- und Pflegeanstalt“ (das heutige PZN_) eingewiesen hat.  Die
Behörden fuhren zu dem Zeitpunkt die akute Phase des NS-Mordprogramms an
psychiatrisierten Menschen, die `Aktion T4`_, nach vor allem kirchlichen
Protesten allmählich zurück und stellten die Massenvergasungen im August
1941 ein.  So überlebte Kunkel noch drei Jahre lang in Wiesloch, bis ihn
das Personal im Juni 1944 gemeinsam mit 171 weiteren PatientInnen
in die weiterbestehende T4-Tötungsanstalt Hadamar_ verlegte.

Das dortige Personal brachte die PatientInnen in dieser Phase vor allem
durch fett- und eiweißfreie Mangelernährung sowie durch Arbeitseinsätze
um; gelegentlich vergifteten die Ärzte die PatientInnen aber auch direkt
mit Luminal_.  Ein Drittel der gemeinsam mit Kunkel verlegten Menschen
starben direkt im Juni 1944, er selbst überlebte die mörderischen
Verhältnisse in Hadamar fünf Monate lang.

Phillip Kunkel starb am 21. November 1944.  Das Personal verscharrte seine
Überreste in einem als Einzelgrab getarnten Massengrab auf dem
Anstaltsfriedhof.  Die Leitung des fortbestehenden psychiatrischen
Zentrums ließ diese Gräber im Jahr 1964 einebnen und den Friedhof
zu einer „Gedenklandschaft“ für die Opfer der Mordfabrik Hadamar
umgestalten.

Quelle: `Loos und Giovannini 2024`_: »Namen, Würde und Erinnerung
bewahren – Dossenheimer „Euthanasie“-Opfer 1940-1945.« Heidelberg:
Kurpfälzischer Verlag.

.. _Loos und Giovannini 2024: https://kurpfaelzischer-verlag.de/wp/product/namen-wuerde-und-erinnerung-bewahren/
.. _PZN: https://de.wikipedia.org/wiki/Psychiatrisches%20Zentrum%20Nordbaden
.. _Aktion T4: https://de.wikipedia.org/wiki/Aktion_T4
.. _Grafeneck: http://de.wikipedia.org/wiki/T%C3%B6tungsanstalt%20Schloss%20Grafeneck
.. _Hadamar: https://de.wikipedia.org/wiki/T%C3%B6tungsanstalt_Hadamar
.. _Carl Schneider: https://de.wikipedia.org/wiki/Carl_Schneider_(Mediziner)
.. _Luminal: https://de.wikipedia.org/wiki/Phenobarbital
