Dietrich August Flamme
======================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: flamme-he-da.jpeg

   Familie Flamme um 1945 (Foto: privat)

Dietrich  Flamme wurde am 4. Juli 1909 in Bochum geboren.  Er arbeitete
zunächst als Bildberichterstatter und Pressezeichner beim Bochumer
Anzeiger.  In seinem späteren Leben arbeitete er auch als Architekt,
Grafiker und Kunstmaler. Er porträtierte  viele  bekannte  Musiker  und
Künstler  wie  Furtwängler,  Bernstein  und Menuhin.

Am 9. Juni 1933 heiratete er in Bochum `Herta Blumenthal`_.  Da sie,
obwohl Protestantin, nach
den Regeln der neuen Regierung Jüdin war, wurde er noch im gleichen Jahr
aus der Reichskulturkammer ausgeschlossen.  Angesichts der düsteren
Perspektiven in Deutschland liquidierte er allen Besitz und versuchte
1934  zwei Mal, in die Schweiz zu  emigrieren.  Die dortigen Behörden
schoben ihn jeweils zurück nach Deutschland ab.  Durch Vermittlung von
`Hans Ehrenberg`_ und `Hermann Maas`_ kam die kleine Familie – seine
Frau hatte am im November 1933 das erste Kind des Paares, Gerda, zur
Welt gebracht – 1935 nach Heidelberg.

Hier arbeitete er als Zeichner für die Jubiläumsschrift zur
550-Jahrfeier der Universität, bis er wieder unter Hinweis auf seine
jüdische Frau entlassen wurde.  1937  erhielt  er  schließlich
absolutes  Berufsverbot. So lebte die Familie fortan unter schwersten
Bedingungen in Heidelberg.

Während der Pogromnacht_ 1938 wohnte die Familie Flamme in der
Lauerstraße und erlebte den Brand der dortigen Synagoge als Augenzeugen
mit.  Dietrich  Flamme widersprach einer älteren Frau, die  sich voller
Hass  über  Juden  äußerte („jetzt müsste man die Juden daneben
aufhängen“).  Daraufhin verhaftete ihn die Heidelberger Polizei und
hielt ihn mehrere Tage im Gefängnis im Faulen Pelz fest.  Dietrichs Frau
zeigte großen Mut,  als  sie,  die  stets  gefährdete  Jüdin, beim
Polizeipräsidenten wegen der Verhaftung  vorstellig  wurde und ihn
freibekam.

.. _Pogromnacht: http://de.wikipedia.org/wiki/Novemberpogrom%201938
.. _Hans Ehrenberg: http://de.wikipedia.org/wiki/Hans%20Ehrenberg%20%28Theologe%29

Am 8. September 1939 bekam das Paar ein zweites Kind, Michael.
Ebenfalls im Jahr 1939 zog die Familie in die Hauptstraße 95.

Nach dem deutschen Angriff auf Polen zog die Wehrmacht Dietrich Flamme
ein; er musste als Berichterstatter  in  Polen  arbeiten,  1941 wurde er
als „jüdisch versippt“ wieder entlassen. Ihm wurde immer wieder
nahegelegt,  sich von  seiner  Frau  zu trennen – was er verweigerte und
ihr so sehr wahrscheinlich das Leben rettete.

Nach seiner Entlassung aus der Wehrmacht  eröffnete  sich  Dietrich
Flamme durch  die  Freundschaft  mit  `Rudi Romhanyi`_, der das
Capitol-Kino betrieb,  ein neues Betätigungsfeld. Er fertigte für das
Kino Bühnenbilder,  Reklameplakate  und  Prospekte  an.  Auch  diese
Tätigkeit  währte nicht  lange,  da  Rudi Romhanyi  verhaftet und als
ungarischer Staatsbürger nach Budapest abgeschoben wurde.

Am 28. November 1940 brachte Dietrichs Frau den jüngsten Sohn des
Paares, Jochen, zur Welt.

.. _Rudi Romhanyi: reich-ru.html

1944 zwangsverpflichtete  die Gestapo Dietrich Flamme zur `Organisation
Todt`_, wo er einer Strafkompanie als Hilfsarbeiter in Frankreich
zugeteilt wurde. In Drillichzeug und Holzpantinen, bei Wassersuppe und
Brot, mussten die Dienstverpflichteten, die eher Gefangenen glichen,
schwerste Arbeit verrichten. Sie wurden zum  „Grotteneinsatz“ (also der
Verlagerung kriegswichtiger Produktion in künstliche und natürliche
Stollen) und zu Bahnkörperreparaturen eingesetzt.

Nach dem Rückzug der Deutschen aus Frankreich wurde Flamme im Lager
Wuppertal-Wichlinghausen zum neuen Strafeinsatz eingeteilt.  Dabei
erkrankte er an einer schweren Diphtherie, Mittelohrentzündung und
Angina.  Am  14.  Januar  1945  gelang  Dietrich Flamme die Flucht aus
dem Lager.


Er tauchte in Tirol unter. Als Mitglied des Alpenvereins war ihm die
Gegend aus früherer Zeit vertraut.  So versteckte er sich an
verschiedenen  Orten  (Ischgl,  Galtür, Trinz, Innsbruck), bis er
schließlich Unterschlupf bei einem Bauern fand. 

Nach der Befreiung 1945 war er der einzige zugelassene Zeichner bei den
Nürnberger Prozessen. 

Als Folge der Erkrankung  und  der  Misshandlungen im Lager litt
Dietrich Flamme an schweren Herzstörungen, die ihn zeitlebens belasteten
und einschränkten. Er starb am  10.  Juni 1999 in Mauer.

`Mehr zur Familie Flamme <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2021/2021-herta-dietrich-flamme.pdf>`_

.. _Herta Blumenthal: flamme-he.html
.. _Organisation Todt: http://de.wikipedia.org/wiki/Organisation%20Todt 
.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Noé: http://de.wikipedia.org/wiki/No%C3%A9%20%28Haute-Garonne%29
.. _Récébédou: http://de.wikipedia.org/wiki/Portet-sur-Garonne
.. _Wiederherstellung des Berufsbeamtentums: http://de.wikipedia.org/wiki/Gesetz%20zur%20Wiederherstellung%20des%20Berufsbeamtentums
.. _Hermann Maas: http://de.wikipedia.org/wiki/Hermann%20Maas%20%28Theologe%29
.. _Polenaktion: http://de.wikipedia.org/wiki/Polenaktion
.. _Nürnberger Gesetze: https://de.wikipedia.org/wiki/N%C3%BCrnberger_Gesetze
.. _Giovannini, Rink und Moraw: https://www.wunderhorn.de/?buecher=erinnern-bewahren-gedenken
.. _Judenhaus: http://de.wikipedia.org/wiki/Judenhaus
.. _Marie Baum: http://de.wikipedia.org/wiki/Marie%20Baum
