Jakob Leonhard
==============

.. container:: dates

  -REPLACE-WITH-DATES-

Jakob Leonhard wurde am 20. Juli 1866 in Waldhilsbach als Sohn des
Ehepaars Jakob und Luise Leonhard  geboren.  Seine Eltern waren Bauern.
Er hatte einen älteren  Bruder,  Georg, und  drei  jüngere
Geschwister, Margarete, Johann und Heinrich Leonhard.

In  seinem  Geburtsort besuchte  Jakob Leonhard die Volksschule. Danach
verließ er Waldhilsbach und begann in Rohrbach eine
Lehre als Bäcker. Allerdings musste er die Lehre schon nach eineinhalb
Jahren  beenden,  da  sein  Vater Unterstützung benötigte.

Mit 27 Jahren heiratete Jakob Leonhard. Das Ehepaar lebte zunächst
für zwei Jahre in  Waldhilsbach und zog  dann  gemeinsam  nach
Heidelberg.

Nach  einem  Aufenthalt  Jakobs in  der
der Heil- und Pflegeanstalt in Wiesloch von April bis Mai 1921 trennten
sich die beiden nach 28 Jahren Ehe.

Als  Rentner  wohnte  Jakob  Leonhard nun  in  einem  kleinen  Zimmer in
Heidelberg. Auch wenn die Miete durch  das  Amt übernommen wurde,
waren die materiellen Verhältnisse sehr bescheiden, wie er selbst
erzählte:

.. pull-quote::

  Ich führe ein armseliges Leben und muss für alles selbst sorgen. Ich
  war heute schon 2 Mal im Geschäft, um Kartoffeln zu holen und habe
  keine bekommen. Ich weiss nicht was ich essen soll. Ich bekomme heute
  mit über 77 Jahren täglich nur 1/4 l Magermilch.  Brot kann ich fast
  nicht essen, weil ich keine Zähne mehr habe. 
  
Jakob  Leonhard  war  meinungsstark  und reflektiert und äußerte sich
wiederholt kritisch über staatliche Vorgänge und politische
Gegebenheiten, weshalb er von der Gestapo
beobachtet wurde. Aufgrund abfälliger Bemerkungen zum Regime und
insbesondere zu Hitler  wurde  er  zunächst  schriftlich  verwarnt.
Nach  einem  weiteren  Kommentar zu Hitler, in dem er ihn als großen
Schwindler bezeichnete, wurde er für zehn Tage in Schutzhaft genommen.

Im  Jahr 1943 beschuldigte ihn eine  Nachbarin
in einer polizeilichen Vernehmung erneut,  
abfällige  Aussagen  über  die Regierung,  in
diesem  Fall  den Propagandaminister
Goebbels,  gemacht zu haben. Er soll nach dem Besuch
Goebbels', der am 9. Juli 1943 anläßlich des Reichsstudententags in
Heidelberg sprach, über ihn gesagt haben: „Ich will ihn
garnicht sehen. Der Schnallentreiber, hätte er doch seine Schnalle
gleich mitgebracht.“ und „Das ist Wahrheit“.

Am 10. September 1943 wurde Jakob Leonhard verhört. Zur „Anschuldigung“
äußerte er sich folgendermaßen:

.. pull-quote::

  Wenn ich gefragt werde, wie ich dazu komme, derartige Äusserungen zu
  machen, so muss ich angeben, dass es in der heutigen Zeit kein Wunder
  ist, wenn man mit den Gedanken auf Abwege gerät.

Aufgrund dieses Vorfalls wurde ein Haftbefehl erlassen wegen des dringenden Verdachts 

.. pull-quote::

  gehässige,  hetzerische  und  von  niedriger Gesinnung  zeugende,
  böswillige  Äusserungen über leitende Persönlichkeiten des Staates und
  der NSDAP gemacht [zu haben], die geeignet waren, das  Vertrauen des
  Volkes zur politischen Führung zu untergraben...

Anschließend an seine Vernehmung wurde er vorläufig festgenommen und in
die Unterbringungshaftanstalt in Wiesloch eingeliefert. Die Haft betrug
acht Monate.  Bei der Entlassung bescheinigte ihm allerdings ein
Amtsarzt Schwachsinn und befand, eine dauerhafte
Unterbringung in der Heil- und Pflegeanstalt sei notwendig.
Jakob Leonhard habe sich, so das Gutachten, durch die mit einer
Schizophrenie  einhergehenden  Symptome staatsfeindlich  geäußert.
Aufgrund  seiner „Geisteskrankheit“ sei es ihm nicht möglich, sich
zurückzuhalten.  Somit stelle er eine Gefahr für die öffentliche
Sicherheit dar und müsse  in  einer  geschlossenen  Heil-  oder
Pflegeanstalt untergebracht werden.

Jakob Leonhard selbst erklärte zu seiner Zeit in der Heil- und
Pflegeanstalt: 

.. pull-quote::
  
  Ich bin nicht als Patient dort gewesen, sondern als Arbeiter. Ich habe
  die ganze Anstalt mit dem elektrischen Licht versehen.

Er selbst scheint sich also nicht als „Patient“ wahrgenommen zu haben
und erklärt, dass er „im früheren Leben immer gesund gewesen“ sei und
noch nie eine  geistige Einschränkung gehabt habe.

Gleichzeitig sollte immer noch eine Gerichtsverhandlung  aufgrund  der
Äußerungen Leonhards bezüglich Goebbels folgen.  An Jakob Leonhard wurde
über die Direktion  der  Heil-  und  Pflegeanstalt  Wiesloch eine
Vorladung  zur  Gerichtsverhandlung des  Landgerichts  in  Mannheim
gesendet.  Die Direktion aus Wiesloch reagierte in einem
Antwortschreiben  auf  die  Vorladung und gab an, dass Jakob Leonhard
aus gesundheitlichen  Gründen  nicht  zur  Hauptverhandlung gebracht
werden könne:

.. pull-quote::

  Das körperliche Befinden des in der hiesigen  Anstalt  untergebrachten
  Rentner  L[.] hat sich in letzter Zeit sehr verschlechtert.  Der
  Kranke ist seit mehreren Wochen bettlägerig, kurzatmig. Es bestehen
  Zeichen von Herzinsuffizienz […] Es ist aus diesen Gründen  leider
  von  ärztlichem  Standpunkt  aus nicht möglich den L. […] zur
  Hauptverhandlung zu verbringen.

Aufgrund der Ausführungen der Direktion der  Heil-  und  Pflegeanstalt
in  Wiesloch wurde das Verfahren eingestellt. Jakob Leonhard wurde 5.
Juni 1944 nach Hadamar_ verlegt, also in eine der zentralen
Vernichtungsstätten des NS-Feldzugs gegen aus seiner Sicht kranke
Menschen.

.. _Hadamar: https://de.wikipedia.org/wiki/T%C3%B6tungsanstalt_Hadamar

Einige Tage nach der Verlegung fragte Margarete  R.,  eine  Schwester
von  Jakob Leonhard, nach seinem Aufenthaltsort und äußerte den Wunsch, ihn bei sich zu Hause pflegen zu wollen. Eine Reaktion auf den
Brief blieb jedoch aus.

In den Akten liegt keine Sterbebenachrichtigung vor. Dennoch kann
aufgrund der Verlegung in die Tötungsanstalt
Hadamar sicher von einer Ermordung Jakob
Leonhards ausgegangen werden.  Das Personal hat diese in der Regel 
unmittelbar nach der Ankunft der Opfer vorgenommen.

`Quelle <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2020/67_Jakob_Leonard.pdf>`_
