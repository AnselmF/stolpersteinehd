Amalie (Mally) Liebhold
=======================

.. container:: dates

  -REPLACE-WITH-DATES-

Amalie (Mally) Liebhold wurde am 24. November 1893 in Bruchsal als
Tochter des Tabakhändlers Kaufmann (Karl) Marx (1863 - 1928) und seiner
Frau Bertha (Berthel) Marx, geb. Gros (1870 - 1943), geboren. Mally
Liebhold ist die ältere Schwester von Ella (Ellen) Flörsheim, geb. Marx
(1896 - 1984), und Anna (Anne, Annele) Fuchs-Marx, geb. Marx (1901 -
2003). 

Sie heiratete 1913 in Bruchsal `Michael Liebhold`_; das Paar hatte drei
Kinder,  `Ruth Liebhold`_, `Martin Liebhold`_ und `Klaus Liebhold`_.
Die Familie Liebhold lebte zunächst zur Miete in der Brückenstraße 51
und seit Anfang der 1920er Jahre im eigenen Haus in der Bergstraße 86 in
Heidelberg. Seit Anfang der 1930er Jahre lebte dort auch Mally Liebholds
Mutter `Berthel Marx`_.

Mally Liebhold, die als Mädchen zum Abschluss ihrer Schulzeit ein
Internat in England besucht hatte, bildete sich schon in den 1920er
Jahren neben ihrer Rolle als Hausfrau und Mutter im Umfeld des
Sanatoriums der Psychoanalytikerin `Frieda Fromm-Reichmann`_ zur
Psychotherapeutin (Psychoanalytikerin) aus. Privat interessierte sie
sich sehr für Musik und Kunst. Sie veranstaltete kammermusikalische
Hauskonzerte, sang selbst gerne und erschuf kleinere Skulpturen aus Gips
und Ton.

Doch setzten die Repressalien der NS-Regierung ab 1933 auch die
Liebholds zunehmend unter Druck.  Schließlich verhaftete die Polizei
ihren Mann nach der Pogromnacht vom 9. November 1938 und verschleppte
ihn in das KZ Dachau_.  Das Personal dort folterte ihn dort wüst.  Zwar
kehrte Michael Liebhold im Dezember 1938 zu Mally zurück, doch starb er
recht bald an den Folgen der Torturen.

Mally Liebhold bereitete daraufhin mit aller Kraft ihre Flucht vor, die
ihr in buchstäblich letzter Minute vor dem deutschen Überfall auf Polen
auch gelang.  Am 31. August 1939 konnte sie in die Niederlande
ausreisen. Dort lebte
sie zunächst zwei Monate zusammen mit ihrer einige Wochen zuvor in die
Niederlande entkommenen Mutter Berthel Marx in Amsterdam.  
Sie wohnten bei Berthels
Tante Marie Schöndorff und deren Mann Albert Schöndorff (1870 - 1942),
die beide bereits 1938 in die Niederlande geflohen waren.

.. _Pogromnacht: http://de.wikipedia.org/wiki/Novemberpogrom%201938

Von den Niederlanden konnte Mally Liebhold im November 1939 an Bord des
Frachters Ajax von Rotterdam über Lissabon nach Genua kommen und von
dort welter in das damalige britische Mandatsgebiet Palästina. Sie lebte
In Jerusalem, vertiefte Ihre psychotherapeutischen Kenntnisse
(Weiterbildung in Jungscher Psychoanalyse) und arbeitete als
Psychotherapeutin.

An Krebs erkrankt und räumlich weit getrennt von ihrer Familie, nahm
sich Mally Liebhold am 26. Dezember 1945 in Jerusalem das Leben. Mally
Liebhold wurde auf dem Friedhof auf dem Ölberg In Jerusalem beigesetzt.

.. _Dachau: http://de.wikipedia.org/wiki/KZ%20Dachau

`Mehr zu Familie Liebhold <http://stolpersteine-heidelberg.de/familie-liebhold.html>`_

.. _Michael Liebhold: liebhold-m.html
.. _Ruth Liebhold: liebhold-r.html
.. _Martin Liebhold: liebhold-ma.html
.. _Klaus Liebhold: liebhold-k.html
.. _Berthel Marx: marx-b.html
.. _Frieda Fromm-Reichmann: https://de.wikipedia.org/wiki/Frieda_Fromm-Reichmann
