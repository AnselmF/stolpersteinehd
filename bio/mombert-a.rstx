Alfred Mombert
================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: mombert-a.jpeg

   Alfred Mombert 1909

Alfred Mombert wurde am 6. Februar 1872 in Karlsruhe als zweites von
drei Kindern des Kaufmanns Eduard Mombert und seiner Frau Helene,
geb. Gompertz, geboren. Nach dem Besuch des Gymnasiums studierte
Alfred Mombert Jura in Heidelberg, Leipzig und Berlin und eröffnete 1899
eine Rechtsanwaltspraxis in Heidelberg.

Schon seit Beginn seines Studiums hatte er begonnen zu dichten: 1894
erschien im Heidelberger Verlag Hörning sein erster Lyrikband „Tag und
Nacht“, der neben liebevollen Familien- und Naturbeschreibungen auch
witzige Großstadtgedichte nicht sehr frivole Liebeslyrik enthielt,
unter anderem auch jene Vision mit dem Titel „Ich saß auf rotem Pfühl im
Prunkgemach“, über die der Dichter schrieb: „incipit creatio“, d.h. die
er als den Beginn seiner schöpferischen Dichtung betrachtete.

Nach dem Ersten Weltkrieg, an dem Alfred Mombert als Unteroffizier
teilnahm, kehrte er zurück nach Heidelberg in seine Wohnung am
Friesenberg 1. Gemeinsam mit seinen Freunden, dem Kulturhistoriker
Richard Benz und dem Maler Gustav Wolf, gründete er „Die Pforte“, eine
Gemeinschaft, deren Ziel es war, in künstlerisch gestalteten
Flugblättern „alte und neue Kunst und Dichtung in einzelnen Blättern zur
Anschauung [zu] bringen … damit die Kunst ins Volk eindringe, um als
eine allen gemeinsame Kultur von ihm wieder zu uns zurückzukehren“.

Zu seinem 60. Geburtstag 1932 gratulierte der Heidelberger
Oberbürgermeister Carl Neinhaus, der ihn später in das
Konzentrationslager Gurs deportieren ließ: „Als Leiter der
Stadtverwaltung Heidelberg empfinde ich es mit freudigem Stolz, daß
unsere Stadt Sie seit Jahren als ihren Bürger zählen darf“.

Zum gleichen Anlass dankte ihm die Berliner Akademie ihm für seine „klar
förderliche Mitarbeit … Sie war uns so gern gewährt, daß wir nicht
bitten brauchen, sie uns weiter zu erhalten: unsere Bitte ist vielmehr
ein stolzer Gruß an Ihre Zukunft“ (Oskar Loerke).  Fünfzehn Monate
später – am 5. Mai 1933 – unterrichtete dieselbe Akademie Alfred Mombert
per Einschreiben, dass er „nach den für die Neuordnung der kulturellen
staatlichen Institute Preußens geltenden Grund­sätzen nicht mehr zu den
Mitgliedern der Abteilung für Dichtung gezählt“ werden könne.

Es wurde einsam um Alfred Mombert. Da sein Verleger Kippenberg zögerte,
„Sfaira der Alte“ herauszubringen, befolgte Mombert den Rat seines
Freundes Martin Buber und wechselte 1935 zum jüdischen Schocken-Verlag.

Im April 1936 erkrankte Alfred Mombert schwer. Seine Freunde Rudolf
Pannwitz, Martin Buber und Gustav Wolf lebten schon im Ausland oder
bereiteten die Emigration vor. An Salman Schocken schrieb Alfred
Mombert 1939: „Wie die Verhältnisse liegen, beabsichtige ich, zumal in
meinem Alter zunächst die Entwicklung weiterhin abzuwarten und nur
stärkster Notwendigkeit nachzugeben.“

Im April 1939 zog seine verwitwete Schwester `Ella Gutman`_ zu ihm in
die Klingenteichstraße. Dort hat die Polizei sie am 22.10.1940 zur
`Wagner-Bürckel-Aktion`_ abgeholt.  Wie die
meisten Jüd_innen in Baden und Pfalz haben die Behörden auch Alfred
Momebert und seine Schwester nach Vichy_ und damit ins
Konzentralionslager Gurs_ abgeschoben.

.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Vichy: http://de.wikipedia.org/wiki/Vichy-Regime

Im November 1940 erreichte den Schweizer Freund Hans Reinhart von ein
Brief von Alfred Mombert:

.. pull-quote::

  Lieber Muri:

  … Es ist mein Schicksal, daß Alles was ich prophetisch
  klangvoll gedichtet habe (zum „ästhetischen“ Genuß der Deutschen), ich
  später in grausamer Realität erleben muß.

  … Wohnung versiegelt durch Gestapo …

  Ich und meine Schwester (72 Jahre alt) samt der gesamten jüdischen
  Bevölkerung Badens und der Pfalz samt Säugling und ältestem Greis ohne
  vorherige Ankündigung binnen einiger Stunden zunächst auf Lastwagen
  zum Bahnhof und dann mittelst Extrazug abtransportiert via
  Marseille-Toulouse zu den Basses Pyrénées, nahe der spanischen Grenze
  in ein großes Internierungslager (Camp de Gurs).

  Bei dem riesigen und ganz plötzlichen Menschenandrang die Verhältnisse
  sehr schwierig und primitiv; kaum etwas zu kaufen. Ganz leichte
  Holzbaracken bei nächtlich kalter Witterung. Jedoch gute Luft (700 m
  Höhe). Man gibt sich anerkennenster Weise große Mühe zu bessern,
  soweit möglich. Meine Schwester ist bei mir, aber getrennt in anderen
  Baracken. Die Zukunft ist völlig dunkel. Wie lange wird dieser Zustand
  dauern können? Wie lange wird man unter gänzlich ungewohnten
  primitiven Verhältnissen durchhalten können? Ob Ähnliches je einem
  deutschen Dichter passiert ist?

  Die schönsten Grüße von „Sfaira dem Alten“

Von Momberts Liebe zu seiner Schwester Ella Gutman, der er schon 1891
sein erstes Gedicht „Erwachen“ widmete, zeugen seine fürsorglichen
Briefe, die er ihr in die Frauenbaracke schickte:

.. pull-quote::

  Liebe Ella! Ich hoffe, daß du mutig durchhältst … Sage mir, was dir
  hauptsächlich fehlt. Falls du noch ein Kopfkissen brauchst, kannst du
  es bekommen, ebenso meine halb- oder ganzwollenen Strümpfe.

Im April 1941 gelang es Freunden um Hans Reinhart in der Schweiz, die
Geschwister zunächst im Internierten-Sanatorium von Idron par Pau
unterzubringen.  „Die Direktion des Hauses hat ein früherer Heidelberger
Arzt, Dr.  Brunswig, der mich dort schon behandelte. Meine Schwester ist
wie neu geboren (bei mir gehört das zum Beruf)“, heißt es in einem Brief
an Hans Reinhart.

In Idron vollendete Mombert weitgehend sein „opus ultimum“ „Sfaira der
Alte“ Teil 2.  Schließlich durften die Geschwister in die Schweiz
ausreisen und erreichten am 11. Oktober 1941 Winterthur, wo sie im Haus
ihres Freundes Hans Reinhart wohnten. Alfred Momberts 70. Geburtstag
wurde in der Literarischen Vereinigung Winterthur gefeiert – Hans
Reinhart überreichte seinem Freund einen Privatdruck von „Sfaira der
Alte“ Teil 2.

Alfred Mombert starb am 8. April 1942 in Winterthur.

`Alfred Mombert in der Wikipedia <https://de.wikipedia.org/wiki/Alfred_Mombert>`_ |
`Mehr zu den Geschwistern Ella Gutman und Alfred Mombert <http://stolpersteine-heidelberg.de/gutman-mombert.html?18/49.4087126/8.7054655>`_

.. _Ella Gutman: gutman-e.html
