Ursula Haug
===========

.. container:: dates

  -REPLACE-WITH-DATES-

Ursula  Haug wurde  am  14.  November 1937 als erstes Kind von Anna und
Karl Haug  in  Neisse in Oberschlesien (heute Nysa, Woiwodschaft Opole)
geboren.  Ihre  Eltern stammten  beide  aus  Heidelberg  –  der Wohnort
in Oberschlesien hatte wohl berufliche  Gründe:  Karl  Haug  war  damals
Schaffner bei der Reichsbahn.

Ursula kam als Zangengeburt zur Welt; wahrscheinlich  war  der
Sauerstoffmangel bei der Geburt ein Grund für die epilepsie-ähnlichen
Anfälle,  die  schon  vier  Monate später auftraten und oft mehrere Tage
lang dauerten.

Im August 1938 zog die Familie zurück nach  Süddeutschland:  zunächst
in  die Nähe von Konstanz, wo Ursulas Schwester geboren  wurde,  dann
nach  Mingolsheim.  Von hier brachten die Eltern die noch nicht
Zweijährige  in  die  Heidelberger  Universitätskinderklinik, dort wurde
sie vom 9. bis zum 16. Oktober 1939 stationär behandelt und die
Diagnose gestellt, Ursula leide an durch organische Anfälle
komplizierten Schwachsinn. Das Kind bedeute eine Gefahr
für seine jüngere Schwester und müsse daher asyliert werden.

Im Herbst 1940 zog die Familie Haug nach Heidelberg in die Krahnengasse
6, der Vater diente inzwischen als Feldwebel bei der Wehrmacht. Anna
Haug beantragte bei der Polizeibehörde die Aufnahme ihrer kleinen
Tochter Ursula in die Heil- und Pflegeanstalt Wiesloch. Um die Kosten
der Unterbringung, die die Familie Haug nicht zahlen konnte, entspann
sich eine minuziöse Korrespondenz zwischen dem Kreiswohlfahrtsamt
Bruchsal und der Polizeibehörde Heidelberg – das Wohl des kleinen
Mädchens findet darin keine Erwähnung. Am 8. August 1940 lieferte ihr
Vater Ursula Haug in die Heil- und Pflegeanstalt Wiesloch
ein.

„Die bisherige Beobachtung, hat sicher gestellt, dass das Kind
schwachsinnig ist. Äussere Ursachen für diesen Schwachsinn sind nicht
wahrscheinlich zu machen,“ heißt es im Aufnahmeprotokoll.

Die Heil- und Pflegeanstalt in Wiesloch (heute: Psychiatrisches Zentrum
Nordbaden, PZN) war seit ihrer Gründung im Jahr 1905 eine Einrichtung
für  Erwachsene,  die  nur  in Ausnahmefällen Minderjährige  aufnahm.
Das änderte sich im August 1939 als der `„Reichsausschuss“`_  mit
Meldebogen  und Meldepflicht für Hebammen, Geburtshelfer und Ärzte den
als „Euthanasie“ verbrämten Massenmord auch an Kindern vorbereitete.

Damals wurde die „Kinderfachabteilung“ in Wiesloch
eingerichtet. Die Bezeichnung „Kinderfachabteilung“ stellt ebenso wie
die des „Reichsausschuss  zur  wissenschaftlichen  Erfassung erb- und
anlagebedingter schwerer Leiden“ eine  Tarnung  dar,  mit  der  Eltern,
Ärzten und Verwaltungen gegenüber vorgetäuscht werden sollte, dass der
nationalsozialistische Staat aufwändige Institutionen unterhalte,  die
sich  mit  der  Versorgung  und Therapie  behinderter  Kinder
befassen.  Tatsächlich  wurden, so Frank Janzowski in seiner
Untersuchung über die NS-Geschichte des PZN , „die  Kinder  eingewiesen,
um dort zu sterben“.

Ursula Haug war das zweite Kind, das in  die  „Kinderfachabteilung“
eingeliefert wurde.  Der  Einweisungsbeschluss,  unterzeichnet von der
Oberin Richter, vermerkt die Unterbringungskosten – pro  Tag zwei
Reichsmark – mit der Bitte an das Kreiswohlfahrtsamt,  die
Kostenübernahme  zu bestätigen.  Außerdem  enthält  der  Bogen eine
handschriftliche Notiz für die Eltern, die gebeten  werden,  einige
Kleidungsstücke und den Religionsnachweis nachzuliefern.  

Zwei Tage nach der Einlieferung füllte der zuständige Anstaltsarzt Dr.
Overhamm den medizinischen Fragebogen aus. Trotz der kurzen Frist
stellte er fest, dass das Kind „schwachsinnig“ sei. Äußere Ursachen für
diesen „Schwachsinn“ konnte er nicht feststellen, weswegen er zur
Diagnose „von Hause aus schwachsinnig“ kam. Das Kind habe „organische
Anfälle“ und sei für sich und andere gefährlich. Ferner sei es
hinsichtlich  Schutz,  Verpflegung  oder  ärztlichen  Beistands
gefährdet  und  benötige eine psychiatrische Behandlung. Die Frage, ob
Ursula heilbar sei, verneinte er, allerdings sei sie „vielleicht bedingt
erziehungsfähig“. Eine Begründung für seine Diagnose, z.B. eine
Verhaltensbeschreibung oder einen  Hinweis  auf  seine  diagnostischen
Methoden,  findet  sich  in  den  Unterlagen nicht, vielmehr begründete
Overhamm die Einweisung der noch nicht dreijährigen Ursula mit ihrer
„allgemeinen Lästigkeit“.

Ausser Ursula Haug befanden sich noch dreizehn weitere Kleinkinder in
der „Kinderfachabteilung“, zwölf waren zugleich sogenannte
„Reichsausschusskinder“.  Auch Ursula wurde am 30. Mai 1941 nach Berlin
gemeldet.  Diese Meldung kam einem Todesurteil gleich. Ermordet  wurden
die Kinder durch Medikamentierung mit Luminal_.
Viermal reiste der „Konsiliarische Tötungsarzt“ Dr. Fritz Kühnke aus
München, wo er in der Heilanstalt Eglfing-Haar schon Erfahrung gesammelt
hatte, zum Morden nach Wiesloch.

.. _Luminal: https://de.wikipedia.org/wiki/Phenobarbital

Ursula Haug starb am 5. August 1941, fast genau ein Jahr nach ihrer
Einweisung in die „Kinderfachabteilung“ der Heil- und Pflegeanstalt
Wiesloch. Als Todeszeitpunkt wurde 18.30 Uhr in die Akten eingetragen,
als Todesursache „Status epilepticus“. Sie wurde zwei Tage später auf
dem Anstaltsfriedhof  beerdigt,  ihr  Grabkreuz  trug  die Nummer 1236.

Die Heil- und Pflegeanstalt stellte die Kosten  für  die  Beerdigung  in
Höhe  von 66,50 Reichsmark dem „Reichsausschuss“ in Berlin in Rechnung.
Wahrscheinlich hat dieser  die  Rechnung  nie  bezahlt  –  Ende August
1941  wurde die  Wieslocher  „Kinderfachabteilung“  aus  Kostengründen
geschlossen.

Dr. Gregor Overhamm wurde nach dem Krieg Anstaltsdirektor der Heil- und
Pflegeanstalt Emmendingen.

Dr.  Fritz  Kühnke arbeitete nach  1945 als  Kinderarzt  und
Kindertherapeut  in Hamburg. 1969 wurde ein Strafverfahren gegen ihn
eingestellt mit der Begründung:

.. pull-quote::

  Kühnke  habe  nicht  aus  rassehygienischen Motiven sondern aus
  Mitleid mit den Behinderten gehandelt, die Kinder seien vollkommen
  schmerzlos in den Tod
  geschlafen.

`Quelle <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2020/65_Ursula_Haug.pdf>`_

.. _„Reichsausschuss“: https://de.wikipedia.org/wiki/Kinder-Euthanasie#%E2%80%9EReichsausschu%C3%9F_zur_wissenschaftlichen_Erfassung_von_erb-_und_anlagebedingten_schweren_Leiden%E2%80%9C
