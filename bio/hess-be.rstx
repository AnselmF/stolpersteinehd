Bernhard Hess
=============

.. container:: dates

  -REPLACE-WITH-DATES-


Bernhard Hess wurde am 4. März 1916 als Kind von Julie_ und `Wilhelm
Hess`_ geboren.  Sein Vater war bei ihrer Geburt bereits fast zwei Jahre
Soldat im ersten Weltkrieg gewesen.  Erst 1918 kehrte er dauerhaft
zurück und setzte seine Vorkriegstätigkeit, Tabakgroßhandel, fort.
Bernhard hatte eine zwei Jahre ältere große Schwester, Doris_, und einen
kleinen Bruder, den im August 1919 geborenen Walter_.

.. _Wilhelm Hess: hess-wi.html
.. _Julie: hess-ju.html
.. _Doris: hess-do.html
.. _Bernhard: hess-be.html
.. _Walter: hess-wa.html

Das zuvor florierende väterliche Geschäft geriet mit der Machtübergabe
an die NSDAP in die Krise, und sein Vater ging schon 1933 nach
Straßburg, um dort für eine Maschinenfabrik seines Onkels zu arbeiten.
Bei einem Besuch in Deutschland im Jahr 1936 geriet der Vater jedoch in
die Fänge der Gestapo, die ihn für die nächsten zwei Jahre im KZ Dachau
internierte.

Bernhard hingegen konnte im Jahr 1938 ein Visum zur Einreise nach
Schanghai erhalten und noch vor der Pogromnacht im November 1938 – nach
der die Behörden seinen kleinen Bruder ins KZ Dachau steckten – über
Paris dorthin fliehen.

Auch in Schanghai mussten jedenfalls viele JüdInnen im Ghetto leben.
Berhard Hess durfte es aber immerhin verlassen, um seiner Arbeit
nachzugehen.  Er verkaufte Feuersteine, die ihm seine Schwester, die
ebenfalls 1938 in die USA hatte fliehen können, aus Chicago schickte.
Zurück in Europa hatten die deutschen Behörden seine Bernhards Mutter
1940 ins Lager Gurs deportiert.  Aus dem Vichy-Französichen Lager konnte
sie Bernhard gemeinsam mit seinen Geschwistern noch freikaufen.  Er
konnte ihr sogar ein Visum für Schanghai verschaffen, doch erreichte
dieses sie zu spät: Deutschland hatte inzwischen die Kontrolle in
Vichy-Frankreich übernommen.  Deutsche Beamte und Soldaten haben
Bernhards Mutter nach Auschwitz deportiert und dort ermorden lassen.

1947 heiratete er noch in Schanghai die österreichische Jüdin Gertrud
Mand (1920–1971), 1948 gingen sie zusammen nach Chicago, wo sich im
Laufe der späten 1940er Jahre die überlebenden Mitglieder der Familie
Hess, also Vater Wilhelm und die drei Kinder, versammelten. Mutter Julie
hatte zwar eine erste Internierung im Lager Gurs_ überlebt; als jedoch
die Deutschen auch im bis 1943 unbesetzten Teil Frankreichs die
Kontrolle übernahmen, hatten sie sie wieder inhaftiert, nach Auschwitz
deportiert und dort ermorden lassen.

Gertrud und Bernhard Hess hatten fünf Kinder: Ronald (1948–1998), Julie
(1950–2023), Susan (geb. 1952), Irene (geb. 1953) und Debbie (geb.
1955).  Nach Erzählungen seiner Kinder hatte Bernhard eine besondere
Begabung: er lernte schnell fremde Sprachen.  So erzählt seine Tochter
Susan, dass er bei einem Arztbesuch plötzlich mit dem Arzt portugiesisch
sprach. Er hatte aus dem Namen des Arztes auf dessen Nationalität
geschlossen.  Seine Kenntnisse des Portugiesischen hatte er in Paris
erworben, als er sich auf eine Emigration nach Portugal vorbereitete,
die jedoch nicht zustande kam. Mit erstaunten Kellnern sprach er bei
Besuchen von chinesisch geführten Restaurants in deren Muttersprache.
Chinesisch hatte er bei seinem 10-jährigen Aufenthalt in Schanghai
gelernt.

Bernhard Hess starb am 7. März 1996 im Lincolnwood, IL, einem Teil der
Metro Area von Chicago.

`Mehr zu Bernhard Hess und seiner Familie <https://stolpersteine-heidelberg.de/mediapool/pdf/2024-fam-hess.pdf>`_

.. _Konzentrationslager Dachau: http://de.wikipedia.org/wiki/KZ%20Dachau
.. _Pogromnacht: http://de.wikipedia.org/wiki/Novemberpogrom%201938
.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
