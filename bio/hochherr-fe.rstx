Ferdinand Hochherr
==================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: hochherr-fe.jpeg

Ferdinand Hochherr wurde 1873 in Berwangen (nahe Eppingen) geboren und
kam 1919/20 mit seinen Brüdern Gustav_ und Simon_ nach Heidelberg.
Gemeinsam mit Simon betrieb er die Tabakfabrik B. Hochherr & Co. GmbH in
der Kaiserstraße.

Die Familien Hochherr lebten in Heidelberg in einem starken
Familienverbund, insbesondere die Familien Ferdinand und Simon Hochherr
waren eng verbunden. Simon war der religiösere von beiden und in der
orthodoxen jüdischen Gemeinde engagiert, Ferdinand dagegen eher liberal.
Beide waren erfahrene, erfolgreiche Geschäftsleute und durch
Einkaufsreisen nach Amsterdam offen und weltgewandt.

Das Fabrikgebäude in der Kaiserstraße 78 steht heute noch (ehemals Betty
Barclay) und war ein nach damaligen Vorstellungen sehr modernes Gebäude.
Es zeigt deutlich die Architektureinflüsse, die die beiden Fabrikanten
aus Amsterdam mit nach Hause gebracht hatten. 

Die Tabakwaren von Hochherr waren in ganz Deutschland bekannt.  Erfolg
hatten die Fabrikanten Hochherr u.a. durch die Herstellung von
„Stumpen“, einer billigen Massenzigarre, die, durchaus von guter
Qualität, nach dem 1.  Weltkrieg bei der verarmten Bevölkerung auf
großen Zuspruch stieß.

In den zwanziger Jahren trafen Ferdinand Hochherr mit dem Tod seiner
ersten Frau Jettchen Hochherr, geb. Ottenheimer (1878 - 1924; sie starb
an den Folgen eines Gehirntumors), und seines Sohnes Jakob (1904 - 1927;
er starb an einer Lungenentzündung) zwei schwere Schicksalsschläge, doch
konnte er zusammen mit seiner zweiten Frau `Eva Mainzer`_ und Jettchens
Kindern Jella_ und Erika_ ein neues Familienleben aufbauen.

Ferdinand Hochherr war ein leidenschaftlicher Schachspieler und
Fahrradfahrer; die Familien Hochherr fuhren auch nach Arosa und
Grindelwald zum Skifahren. Die Hochherr-Brüder waren sich jedoch als
wohlhabende Männer auch ihrer sozialen Verantwortung bewusst und halfen
innerhalb und außerhalb der Familie.

1937 beschäftigte die Firma 300 ArbeiterInnen und 15 Angestellte. Der
Zigarrenumsatz betrug ca. 400000 RM, der Umsatz mit den Stumpen und
sonstigen Produkten ca. 2 Millionen RM.  Doch war seit der Machtübergabe
an die Nationalsozialisten der Druck auf die Firma wegen ihrer jüdischen
Eigentümer immer weiter gewachsen, und am 17.5.1938 mussten sie ihre
Firma vertraglich abtreten („Arisierung“).

Während der Pogromnacht_ vom 9.11.1938 war er in Berlin, wo er die
unmittelbare Gewalt unerkannt überstand.  Nach seiner Rückkehr nach
Heidelberg deportierten ihn die Behörden – wie fast alle männlichen
Juden – in das `Konzentrationslager Dachau`_.  Er selbst vermutete, er sei
im Wesentlichen als Geisel genommen worden, um die Auslieferung des
Vermögens der `B'nai B'rith`_-Loge – er war deren Vizepräsident – an den
faschistischen Staat zu erzwingen.  In der Tat kam er offenbar bereits
nach einer Nacht wieder frei – aber er sprach nie mehr von dieser Nacht,
und er betrieb daraufhin mit aller Kraft die Auswanderung seiner
Familie.

.. _Pogromnacht: http://de.wikipedia.org/wiki/Novemberpogrom%201938
.. _B'nai B'rith: http://de.wikipedia.org/wiki/B%27nai%20B%27rith
.. _Konzentrationslager Dachau: http://de.wikipedia.org/wiki/KZ%20Dachau

Nach der Enteignung – Reichsfluchtsteuer, Judenvermögensabgabe,
Judenauswanderungsabgabe usw. hatten das Vermögen zusätzlich
zusammenschmelzen lassen – boten sich für Ferdinand Hochherr mit Familie
die Niederlande als Zufluchtsort an. Hier kannte er sich aus, hatte
viele geschäftliche Kontakte und gute persönliche Beziehungen. Am
11.1.1939 emigrierten Ferdinand und seine Frau Eva nach Amsterdam, wohin
die Tochter Erika mit ihrem Mann schon 1936 geflohen war und wo sich
auch Simon Hochherr mit Familie einfand. 

Als die Wehrmacht 1940 die Niederlande besetzte, wurden nach und nach die
gleichen Beschränkungen wie in Deutschland eingeführt, die Sicherheit,
die sich die Familie erhofft hatte, war trügerisch. Die Besatzer
deportierten Ferdinand und Eva in das Durchgangslager Westerbork_.
Von dort verschleppten die Behörden Ferdinand Hochherr am 10.3.1943 in
das Vernichtungslager Sobibor und ließen ihn dort am 13.3.1943 ermorden.

`Mehr zur Familie von Ferdinand Hochherr <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2012/2012_Fam_Ferdinand_Hochherr.pdf>`_

.. _Gustav: hochherr-gu.html
.. _Simon: hochherr-s.html
.. _Eva Mainzer: hochherr-ev.html
.. _Jella: hochherr-je.html
.. _Erika: hochherr-er.html
.. _Westerbork: http://de.wikipedia.org/wiki/Durchgangslager%20Westerbork
