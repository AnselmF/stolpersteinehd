Leo Leser Heiselbeck
====================

.. container:: dates

  -REPLACE-WITH-DATES-

Leo Leser Heiselbeck wurde im Juli 1910 in Köln als Sohn von Cilli
Heiselbeck geboren, die ihrerseits 1884 im westpolnischen Żmigród
geboren worden war.

Er heiratete Mitte der 1930er Jahre `Fanny Heiselbeck`_ aus Heidelberg,
deren Familie in der Heinrich-Fuchs-Straße 41 wohnte.

Wegen der Herkunft seiner Mutter schoben ihn die deutschen Behörden im
Zuge ihrer „Polenaktion_“ am 28. Oktober 1938 nach Polen ab.  Auch sein
Schwiegervater `Jakob Isaak Storch gen. Stern`_ hatte die polnische
Staatsbürgerschaft und wurde in denselben Transport gesteckt.  Die
polnische Seite brachte sie in einem Lager bei Zbaszyn (Bentschen)
gleich hinter der Grenze unter.  Während sein Schwiegervater recht bald
in das Ghetto nach Gorlice umzog, blieb Leo in Zbaszyn, bis im Juli 1939
auch seine Frau  nach Polen ausgewisen wurde. 

Die beiden zogen nach Krakau.  Auf einer Postkarte vom 31. Dezember 1939
steht ihre Adresse: Krakau, Krakowska Straße 18 im alten jüdischen
Viertel Kazimierz. Im März 1941 mussten alle jüdischen BewohnerInnen
Krakaus in das neu ausgewiesene Ghetto Podgorze umziehen. Die deutschen
Behörden ließen den Stadtteil mit Mauer und Stacheldraht abriegeln und
seine BewohnerInnen in der Folge fast alle ermorden. 

In der Wiedergutmachungsakte seiner Schwägerinnen Lea Muniches/Eva
Rothschild in Kopie vorhandene Postkarten berichten davon, dass Leo und
Fanny noch von Krakau aus eigene Bemühungen für eine Ausreise über das
amerikanische Konsulat in Warschau anstrengen wollten, wobei sie Zweifel
äußern, ob das Konsulat noch „amtiert“. 

.. figure:: heiselbeck-karte.jpeg
  :figclass: full

  Postkarte von Fanny und Leo Heiselbeck vom 21. Dezember 1939 aus
  Krakau an seine Schwägerin Eva und deren Mann Sali Rothschild.

Eine letzte Nachricht findet sich auf einer Postkarte vom 4. März 1940.
Leos im Ghetto Gorlice lebende Schwiegereltern schreiben darin: „Von Leo
und Fanny haben wir vorige Woche [also im Februar 1940] ein Schreiben
bekommen, daß sie sich soweit wohl befinden.“ Danach gibt es keine
Spuren der Eheleute Heiselbeck mehr. Sie gelten als verschollen.

`Mehr zur Familie von Leo Heiselbecks Frau <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2016/2016_Storch_Ziegler_Heiselbeck.pdf>`_

.. _Polenaktion: https://de.wikipedia.org/wiki/Polenaktion
.. _Paula Storch: storch-p.html
.. _Jakob Isaak Storch gen. Stern: storch-ji.html
.. _Klara: ziegler-k.html
.. _Ester: ziegler-e.html
.. _Fanny Heiselbeck: heiselbeck-f.html
