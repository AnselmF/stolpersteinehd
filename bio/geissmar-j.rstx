Dr. Johanna Geißmar
===================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: geissmar-j.jpeg

Johanna Geißmar wurde am 7. Dezember 1877 in Mannheim als jüngstes Kind
von Josef Geißmar, einem angesehenen Rechtsanwalt in Mannheim, und seiner
Ehefrau Clara Regensburger, gebürtig aus Eppingen, geboren.  In Mannheim
besuchte sie die Höhere Töchterschule und half ihrer Mutter Clara bei
der Führung des Haushaltes. Nach dem Tod des Vaters holte Johanna
Geißmar auf dem humanistischen Gymnasium „Hohenbaden“ in Baden-Baden das
Abitur nach und immatrikulierte sich im Oktober 1909 an der
Ruprecht-Karls-Universität für Medizin (die Heidelberger
Universität ließ seit 1900 auch Frauen zum Studium zu).

In dieser Zeit lebte sie im Hause ihres Bruders Jakob_, der
Landgerichtsrat war, im Graimbergweg 1 oberhalb der Altstadt. Zu Beginn
des 1. Weltkrieges meldete sie sich freiwillig für den Lazarettdienst,
nach dem sie im April 1920 wieder nach Heidelberg zog und eine
Kinderarztpraxis eröffnete, zunächst in der Erwin-Rohde-Straße 11a, dann
ab dem 26. März 1927 in der Moltkestraße 6, wo sie auch wohnte. Durch den
Bekanntheitsgrad der ganzen Familie Geißmar in Heidelberg, insbesondere
ihres Bruders Jakob, hatte sie bald einen großen PatientInnen-Stamm.

Seit 1930 jedoch nahm der Zustrom ihrer PatientInnen unter dem Druck der
NS-Propaganda ab. Nach der Machtübertragung an die NSDAP im Januar 1933
übernahm im März der gefürchtete Gauleiter Wagner die Regierungsgewalt
in Baden. Schon im Boykottaufruf der Gauleitung am 1.4.1933 wurde die
Bevölkerung aufgefordert, u.a. auch die jüdischen Ärztinnen und Ärzte zu
meiden. Kurz danach wurde am 22. April in Baden allen jüdischen
Ärztinnen und Ärzten die Zulassung zur Kassenarztpraxis entzogen. Ende
April erhielt auch Dr. Johanna Geißmar, die nun schon 55 Jahre alt war,
das Schreiben zur sofortigen Beendigung ihrer kassenärztlichen
Tätigkeit.

Da sich in ihrem PatientInnen-Stamm nur wenige SelbstzahlerInnen
befanden, sah sie finanziell keine Möglichkeit, ihre Praxis
fortzuführen. Gleichzeitig hoffte sie, durch Rückzug in ein ländliches
Gebiet den zunehmenden Verfolgungen und Repressionen entgehen zu können.
Sie zog zunächst nach Bärental am Feldberg im Schwarzwald.  Ab 1935 lebte
sie dann in Saig, wo auch ihr Bruder Friedrich zu ihr stieß, der seine
Arztpraxis in Mannheim trotz immer größerer Einschränkungen und
Schikanen bis 1938 weitergeführt hatte.

Doch auch hier im Schwarzwald gab es genug AntisemitInnen, die sie
denunzierten und nach dem Novemberpogrom 1938 auch tätlich angriffen.

Auch Mitglieder der bekennenden Kirche konnten sie nicht schützen. In
der immer auswegloseren Situation nahm sich Friedrich Geißmar im
Frühherbst 1940 in Saig das Leben, kurz vor der Massendeportation der
badischen Jüdinnen und Juden am 22.10.1940 (`Wagner-Bürckel-Aktion`_).
Seine Leiche wurde im Frühjahr 1941 im Wald bei Saig gefunden.

.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion

Hingegen verschleppten die Behörden Johanna Geißmar nach Gurs_, wo sie,
nunmehr schon über 60 Jahre alt, unter extremsten Bedingungen im
Frauenlager arbeitete, um den vielen Kranken, Verletzten und Sterbenden
zu helfen.  Hilfsmittel, Verbandsmaterial und Medikamente hatte sie fast
keine, doch durch ihren Einsatz vermittelte sie den Gefangenen ein Stück
Geborgenheit. Gemeinsam mit der Krankenschwester Pauline Maier aus
Baiertal pflegte sie die Menschen im Lager bis zur Erschöpfung. In dem
Film „Engel in der Hölle“ setzte ihnen Dietmar Schulz in der Reihe
„ZDF-History“ ein Denkmal.

Am 12.8.1942 begleitete Johanna Geißmar einen Todestransport mit ihren
Schützlingen nach Auschwitz, wo sie am 14.8.1942 umgebracht wurde. Sie
stand nicht auf der Liste für diesen Transport, französische KollegInnen
wollten sie zurückhalten. Doch sie wollte die Menschen nicht ohne ihre
ärztliche Hilfe alleine gehen lassen und hatte wohl ein letztes Fünkchen
Hoffnung, ihren aus München deportierten Bruder Jakob_ und seine Frau
Elisabeth zu finden (die zu dem Zeitpunkt noch in Theresienstadt
eingesperrt waren; Elisabeth_ und Johannas Nichte Martha_ wurden im
Oktober 1944 nach Auschwitz deportiert und ermordet).

In Saig hat Richard Zahlten Johanna Geißmar mit seinem Buch „Meine
Schwester starb in Auschwitz“ ein bewegendes Andenken bewahrt und
erreicht, dass die Gemeinde kurz nach der Jahrtausendwende einen
Gedenkstein am Saiger Hochfirstweg errichtet hat.

`Quelle <http://stolpersteine-heidelberg.de/johanna-geissmar.html>`_;
`mehr zur Familie Geißmar <http://stolpersteine-heidelberg.de/familie-geissmar.html>`_

.. _Jakob: geissmar-ja.html
.. _Elisabeth: geissmar-e.html
.. _Martha: geissmar-m.html
.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
