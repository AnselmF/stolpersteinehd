Harald Gumbel
=============

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: gumbel-ml-ha.jpeg
  :alt: Eine Frau Anfang 40 und ein halbwüchsiger Junge sehen, einander
    zugewandt, in die Kamera.

  Harald mit seiner Mutter in Lyon, 1935 (Foto aus: Harold Gumbel:
  Memories, 2019).

Harald Gumbel kam am 14. April 1921 als Sohn von Eduard und `Marie
Luise Solscher`_, geborene von Czettriz, zur Welt. Er hatte einen großen
Bruder, Jürgen.

.. _Marie Luise Solscher: gumbel-ml.html

Haralds Mutter entstammte einer Dynastie preußischer Offiziere, doch
hatte sich sie sich unter „dem Einfluss ihrer [also Marie Luises]
rebellischen Mutter“ (Harald) zur Pazifistin entwickelt.  1926, Harald
war noch nicht in der Schule, zog sie mit ihm nach Heidelberg, um als
Privatsekretärin des Mathematikers und Pazifisten `Emil Julius Gumbel`_
zu arbeiten.  Sein großer Bruder blieb beim Vater.

.. _Emil Julius Gumbel: gumbel-ej.html

Mutter und Sohn wohnten zunächst im Kapellenweg, und Harald besuchte die
Volksschule in Neuenheim. Später wechselte er auf die Realschule in der
Kettengasse, das heutige Helmholtzgymnasium, dessen rigiden
Turnunterricht er in seinen Erinnerungen schildert.

1930 heirateten Gumbel und Haralds Mutter, und Gumbel adoptierte Harald;
aus „Onkel Gumbel“ wurde „Pappi“ und aus Harald Solscher Harald Gumbel.

Haralds Erinnerungen an seine Heidelberger Zeit sind zwiespältig: Zum
einen Picknick-Ausflüge mit Freunden, Skiferien in den Alpen, aber auch
Terror rechter Studenten gegen das Haus in der Beethovenstraße und ein
Überfall der Hitlerjugend auf ihn in der Straßenbahn.

1932 floh die Familie vor den künftigen Machthabern in Deutschland, auf
deren Feindeslisten Haralds neuer Vater ziemlich weit oben stand.  Sie fanden
eine Zuflucht in Frankreich, zunächst in Paris, dann in Lyon.

Für Harald war der Wegzug aus Heidelberg eine Befreiung: Seine
Erinnerungen an die Jahre in Frankreich überschrieb er „Et in Arcadia
Ego“. Es sind seine Teenagerjahre, die er mit seiner Familie, zu der
inzwischen auch die verwitwete Großmutter gehörte, vor allem in Lyon
verbrachte – im Winter skifahrend in den Alpen, im Sommer häufig in
Sanary sur Mer, in der Nachbarschaft von Lion Feuchtwanger und anderer
Prominenz.

„Arkadien“ endete mit dem Einmarsch der Deutschen 1940: „Emigrance once
more“ heißt das Kapitel einer abenteuerlichen Flucht durch Frankreich, bis er im Juni 1941 New York erreichte und seine Familie wieder traf.

Nach dem Studium am Massachusetts Institute of Technology arbeitete er vor allem für die militärische Luftfahrt. Mit der amerikanischen Staatsbürgerschaft änderte er seinen Vornamen in Harold.

Über seine Arbeit in der Rüstungsforschung schrieb er 1971:

.. pull-quote::
  Due to the writer's professional activities, the majority of his work
  was classified and received only restricted publication.

Die HerausgeberInnen seiner „Memories“ fügen hinzu:

.. pull-quote::
  Life is something strange. It is somewhat ironic or paradoxical that
  the son of the pacifist Emil J. Gumbel became an aeronautical engineer
  in the industrial military complex in the USA.

Harold Gumbel war in zweiter Ehe mit Patricia Gumbel verheiratet. Er
starb im Jahr 2016 kurz vor seinem 95. Geburtstag im kalifornischen Palo Alto.
