Gustav Hochherr
===============

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: hochherr-ufer20.jpeg

  Die Familie Weil vor der Uferstraße 20, ganz links vermutlich
  Gustav Hochherr

Gustav Hochherr wurde am 2. März 1872 in Berwangen geboren, einem Dorf im 
Kraichgau in der Nähe von Eppingen. Er hatte drei Brüder, Moritz,
Ferdinand_ und Simon_. 

Er war verheiratet mit `Frieda Hochherr`_, die 1906 die gemeinsame Tochter
`Ilse Hochherr`_ (verh. Weil) zur Welt brachte, 1912 dann `Alice
Charlotte`_.

Gustav Hochherr war Kaufmann und Mitinhaber der Firma Levi Hochherr,
Rohtabakhandel, die er seit 1928 zusammen mit seinem Schwiegersohn
`Arthur Weil`_ führte. Sitz der Firma war die Uferstraße 20. Dort
wohnten die Familien der beiden Geschäftsführer auch.

In der Pogromnacht_ 1938 griffen Heidelberger Bürger Haus und
Firmenräume an, die Polizei verhaftete am Folgetag Gustav Hochherr und
seinen Schwiegersohn.  Während die letzteren in das Konzentrationslager
Dachau_ deportierten, ließen sie Gustav Hochherr als einen der wenigen
männlichen Juden in Heidelberg wieder frei, möglicherweise aufgrund
seines Alters.  Seine Firma wurde jedoch noch 1938 liquidiert.

Die Behörden zwangen Gustav Hochherr am 22. Oktober 1940 in den
Transport der meisten in Baden verbliebenen JüdInnen in das
Konzentrationslager Gurs_ (`Wagner-Bürckel-Aktion`_).  Die furchtbaren
Verhältnisse im Lager brachten den fast 70-Jährigen im zweiten Winter
um.  Er starb am 21. Dezember 1941.

`Mehr zu den Familien Hochherr und Weil
<http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2012/2012_Fam_Gustav_Hochherr_Arthur_Weil.pdf>`_

.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Ferdinand: hochherr-fe.html
.. _Simon: hochherr-s.html
.. _Arthur Weil: weil-a.html
.. _Frieda Hochherr: hochherr-fr.html
.. _Alice Charlotte: hochherr-al.html
.. _Ilse Hochherr: weil-il.html
.. _Pogromnacht: http://de.wikipedia.org/wiki/Novemberpogrom%201938
.. _Dachau: http://de.wikipedia.org/wiki/KZ%20Dachau
.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
