Lucia Östringer
===============

.. container:: dates

  -REPLACE-WITH-DATES-

Lucia Östringer wurde am 20. März 1933 in Heidelberg als Tochter von
Berta Östringer geboren, aufgewachsen ist sie in Rettigheim.

Am 25. Januar 1937 wurde sie ins
Paulusheim Heidelberg aufgenommen, das in der Plankengasse 2 stand. 
Am 7. Februar 1937 kam sie in die „Erziehungs- und Pflegeanstalt für
Geistesschwache“ in Mosbach und wurde von dort am 17. September  1940
in  die  Landespflegeanstalt Grafeneck_ überstellt, deren Personal sie
noch am gleichen Tag in der Gaskammer ermordet hat. 

Hier folgt nun ein fiktiver Brief ihrer Familie an Lucia:


Liebe Lucia,

wir wissen nicht viel über dich. Es gibt kein Foto von dir und in deiner
großen Familie wurde  auch  nicht  viel  über dich  gesprochen. Nur eine
deiner Tanten erzählte ihrer Tochter, du seist ein Kind mit schönen,
langen, blonden Locken gewesen. Leider hattest du vermutlich eine
schwere körperliche und geistige Behinderung.

Auch deine Mutter Berta hat nie über dich gesprochen. Sie war das zehnte
von zwölf Kindern und selbst geistig behindert.  Um ihren 17. Geburtstag
herum wurde sie schwanger. Dein Vater war wahrscheinlich ein junger
Mann, der auch als geistig zurückgeblieben  galt.  Deine  Mutter  Berta
hatte  keine  Beziehung  zu  ihm,  aber  sie kannte ihn. Nach vielen
Jahren traf sie den Mann  auf  der  Straße. Sie zeigte auf ihn und sagte
zu ihrer Begleitung: „Das ist er!“

Ansonsten verlor deine Mutter Berta keine Silbe über den Kontakt zu ihm.
Sie verstand  sicher  auch  nicht  den  Zusammenhang  zwischen  der
Zeugung  und  deiner späteren Geburt. Der Familie aber fiel Bertas
Gewichtszunahme auf. Bertas Vater befragte sie eingehend und erfuhr so
von der Schwangerschaft. Wohl bald danach wurdest du am 20. März 1933 in
der Frauenklinik in Heidelberg geboren und dort am 28.  März  1933
katholisch  getauft.  Deine Mutter war anscheinend durch diese Geburt
traumatisiert  und  musste  einige  Tage  in der
Psychiatrisch-Neurologischen  Klinik  in Heidelberg verbringen. 

Dein Erzeuger hatte keinen Kontakt zu dir. Für seine Tat wurde er nie
zur Rechenschaft gezogen.

Die Großfamilie deiner Mutter lebte in einer  Gemeinde  bei  Heidelberg.
Deine Großeltern  hatten  ein  hartes,  arbeitsreiches  Leben  mit ihrer
Landwirtschaft  und zudem  war  dein  Großvater  auch „Heiligrechner“
[#hr]_ im Heimatdorf. Als du dort aufgenommen wurdest, lebte außer den
Großeltern nur noch ein Teil deiner Onkel und Tanten im Elternhaus.
Einige hatten schon ihre eigenen Familien gegründet. So versorgten deine
Mutter Berta und der Rest der Familie dich, die kleine Lucia. Vermutlich
wurdest du nur im Haus gepflegt und kamst nie nach draußen. Als im Mai
1936 deine  Großmutter starb, übernahmen die überwiegend berufstätigen
Familienmitglieder deine Pflege.

Den größten Einschnitt in deinem Leben brachte 1937  die
Zwangssterilisation deiner  Mutter  Berta.  Während ihres  Aufenthalts
in der Frauenklinik Heidelberg, wo der  Eingriff  vorgenommen  wurde,
kamst du zur kurzfristigen Unterbringung in das Paulusheim  in
Heidelberg,  da,  wie  dein Großvater  dem  Erbgesundheitsgericht
schrieb,  sich  in  dieser  Zeit  niemand  um deine  Versorgung  kümmern
könne.  Alle noch  im  Hause  verbliebenen  Onkel  und Tanten würden
ihrer Arbeit nachgehen. 

Schon am 7. Februar 1937 folgte deine Verlegung in die „Erziehungs- und
Pflegeanstalt für Geistesschwache“ in Mosbach.  Wahrscheinlich  wurdest
du  im  dortigen „Krankenhaus“  gepflegt,  wo  Kinder  mit schwerer
körperlicher und geistiger Behinderung untergebracht waren.

Dein Erzeuger wurde nicht zwangssterilisiert. Er hat später eine eigene
Familie gegründet.

Dein  Großvater  war  zwischenzeitlich schwer erkrankt und starb an
seinem Geburtstag am 17. Juni 1937. So bliebst du dauerhaft in Mosbach.
Deine Onkel haben dich dort regelmäßig besucht.

Eines Tages wurde der Familie mitgeteilt, du seist an den Folgen einer
Diphtherieerkrankung verstorben. Die Familie hat das nie geglaubt. Sie
dachte schon damals, dass  du  das  Opfer  des  Euthanasieprogramms
geworden sein könntest. So wurde es innerhalb der Familie weitergegeben
und nur sehr wenige Nachkommen erfuhren überhaupt von dir.

Forschungen zur Geschichte der Anstalt Mosbach und ihrer Bewohner haben
ergeben,  dass  1940  in  drei  Transporten  217 Menschen  von  Mosbach
abtransportiert und in der Vernichtungsanstalt Grafeneck auf der
Schwäbischen Alb ermordet wurden.  Insgesamt  fanden  in  Grafeneck  im
Jahre 1940 im Rahmen der sogenannten `Aktion T4`_ über 10.000
behinderte Menschen den gewaltsamen Tod im Gas. Dokumente belegen, dass
auch du am 17. September  1940  nach  Grafeneck  verbracht und noch am
gleichen Tag in der Gaskammer dort  ermordet  wurdest.  So  hat  sich
nach  über  siebzig  Jahren  die  Familienlegende als Wahrheit
herausgestellt. Du, das kleine Mädchen Lucia Östringer, wurdest Opfer des
Massenmords der Nationalsozialisten  an  Menschen  mit  Behinderung.  Du
wurdest nur 7 ½ Jahre alt.

Dieser Brief  soll  an  dich  und  an das Schicksal so vieler Anderer
erinnern.

`Quelle <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2020/47_Oestringer_Text.pdf>`_

.. _Aktion T4: https://de.wikipedia.org/wiki/Aktion_T4
.. [#hr] Ein alter Begriff für eine Person, die Kirchensteuer einzog
