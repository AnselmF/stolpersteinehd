Dr. Sigmund Weinberger
======================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: weinberger-si.jpeg

   Sigmund Weinberger (Leo Baeck Institut, DigiBaeck_ AR 25541)

Sigmund  Weinberger wurde  am  2.  März 1878 in Schriesheim als erster
Sohn des Handelsmanns  Benjamin  Weinberger  und seiner Ehefrau Bertha
geb. Behr geboren.  1880 kam sein Bruder Adolf zur Welt.  

Sigmund besuchte nach der Volksschule  das Gymnasium in Heidelberg.
Danach studierte er an der Universität Heidelberg Medizin  und wurde
1903 unter  Prorektor Vincenz Czerny mit magna cum laude zum Dr.  med.
promoviert.  Vor  dem ersten Weltkrieg arbeitete er an Krankenhäusern in
Wiesbaden  und  Königsberg  und  von  1907  bis 1914 in Rastatt. 
Am 19. März 1914 heiratete er `Selma  Kaufmann`_.

Das Paar hatte zwei Kinder, Bertha und Benjamin.

Von August 1914 bis November  1918  arbeitete  er  als  Stabsarzt  an
der Westfront, längere Zeit in `Saint-Quentin`_ im Norden Frankreichs.

.. _Saint-Quentin: https://de.wikipedia.org/wiki/Saint-Quentin

Nach  dem  Krieg  ließen sich  die Weinbergers in Heidelberg nieder.
Sigmund Weinberger praktizierte von Dezember 1919 bis Dezember 1936 in
der Rohrbacher Straße 43.  Er  war  Eigentümer  des  Hauses, in dem auch
seine Schwiegereltern Ferdinand und Jeanette Kaufmann wohnten.

Sein  Einkommen  ging nach seinen Angaben im späteren
Wiedergutmachungsverfahren bereits während der Wirtschaftskrise ab 1929
kontinuierlich zurück: 1930 betrugen die Jahreseinkünfte  noch  15.000
RM, 1931 noch 13.700 RM, 1932 noch 10.700 RM.  Mit der Machtübergabe an
die NSDAP und der folgenden Hetze gegen JüdInnen brachen seine Einnahmen
drastisch ein und beliefen sich 1933 nur noch auf 3.800 RM und waren bis
1936 weiter auf 400 RM geschrumpft. Immerhin konnte er als
Kriegsteilnehmer und Kriegsgeschädigter – im Gegensatz zu anderen
behördlicherseits als jüdisch klassifizierten ÄrztInnen – bis Ende 1936
noch eine allgemeine Kassenpraxis führen.

Zum 1. Dezember 1936 gab Sigmund Weinberger  seine  Praxis  schließlich
auf  und  floh im Januar 1937 mit seiner Frau Selma auf der SS Manhattan
von Hamburg aus in die USA.  Er konnte eine ansehnliche
Wohnungseinrichtung sowie die Ausstattung des Sprechzimmers  mit
medizinischen  Instrumenten und Apparaten mitnehmen.  Für die Emigration
konnte  Weinberger noch die Reichsfluchtsteuer_ in  Höhe  von  28.000
Reichsmark aufbringen.

.. _Reichsfluchtsteuer: https://de.wikipedia.org/wiki/Reichsfluchtsteuer

Das  Haus  in  der  Rohrbacher Straße  scheint  nicht  verkauft worden
zu sein. Es konnte nicht ermittelt werden, wie und von wem es weiter
verwaltet wurde.  Nach Weinbergers Angaben verfiel es  aufgrund der 11.
Verordnung zum Reichsbürgergesetz vom 25. November 1941 dem Reich.

Die Weinbergers kamen am 25. Januar 1937 in New York an.  Bei der
Passkontrolle erlitt Selma eine Herzembolie und starb noch während der
Einreise.

Sigmund  Weinberger lebte in den USA zunächst im Haushalt seines
Schwagers Max Kaufmann.  Er heiratete am 11. September 1938 die seit
1932 verwitwete Erna Katz, geb. Behr.  Im Oktober 1938 ließ er sich
wieder als Arzt nieder, erreichte jedoch erst ab 1943 ein normales
Einkommen.

Er führte in den 50er Jahren ein Entschädigungsverfahren gegen die
Bundesrepublik Deutschland.  Ihm wurde schließlich erstattet:

* für Schaden an  Vermögen:  Auswanderungskosten und Verlust der
  Arztpraxis: 9.813,92 DM
* für Schaden durch  Zahlung  von Sonderabgaben,  hier
  Reichsfluchtsteuer: 5.600,00 DM
* für Schaden im beruflichen Fortkommen  (Kapitalentschädigung und
  Rentenzahlungen, hier eingetragen bis 1957; danach weiter monatliche
  Rentenzahlung): 33.000,00 DM

Neben  der  Entschädigung erhielt  Sigmund  Weinberger  das  dem
Deutschen Reich  verfallene  Haus  durch  das  Land Württemberg-Baden
als  Rechtsnachfolger des Reiches wieder zurück und konnte es nach
einigen Jahren verkaufen. Auch die entgangenen Mieteinnahmen wurden nach
den Bestimmungen des  Bundesrückerstattungsgesetz erstattet. Für
entgangene Nutzungen des Grundstücks Rohrbacher Straße 43 in Heidelberg
in Höhe von 6.609,45 RM erhielt Weinberger 660,94 DM.

Sigmund Weinberger starb am 13. Dezember 1968 im New Yorker Stadtteil
Jackson Heights.

`Mehr zum Ehepaar Weinberger <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2020/27_Weinberger.pdf>`_

.. _Selma  Kaufmann: weinberger-se.html
.. _DigiBaeck: https://www.lbi.org/collections/digibaeck/
