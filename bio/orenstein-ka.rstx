Käthe Orenstein
===============

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: orenstein-ka.jpeg

   Käthe Orenstein um 1933 (Foto: privat)

Karoline Orenstein, genannt Käthe, wurde am 21. Juli 1918 in Heidelberg
gemeinsam mit ihrer Zwillingsschwester Fay_ geboren.  Ihre Eltern
waren Selma_ und `Heinrich Orenstein`_, die ein An- und Verkaufsgeschäft
betrieben und ab 1922 in der Plöck 10 wohnten.  Ihre große Schwester,
Zita_, war fast zehn Jahre älter als sie, ihre Mutter brachte ihre
kleine Schwester Cilly_ 1925 zur Welt.

Gemeinsam mit ihrer Zwillingsschwester besuchte sie das
Mädchenrealgymnasium in der Plöck, das heutige Hölderlingymnasium.
Während der ersten Jahre der Herrschaft der NSDAP wuchs der Druck auf
den elterlichen Betrieb, so dass ihn ihr Vater 1937 aufgab.  Kurz
darauf starb er.

Während Zita schon 1935 in das Mandatsgebiet Palästina und Fay im
Juni 1938 nach England hatten fliehen können, fand sich für Käthe
keine Fluchtmöglichkeit.  Am 12. Mai 1939 haben ihr die Heidelberger
Behörden einen Fremdenpass ausgestellt, gültig für zwei Jahre.  Er wurde
einmalig für ein weiteres Jahr verlängert. Käthe wird in der
Karteikarte, auf der  diese  Notizen festgehalten  sind, als
Haushaltspraktikantin bezeichnet.  

In  welchem  Haushalt  sie  als  solche  gearbeitet hat, ist unklar.
Belegt ist aber ihr Aufenthalt in Bielefeld, wo sie wohl in einem Lager
war, in dem junge Menschen auf die Auswanderung  nach  Palästina
vorbereitet wurden. Es gab mehrere solcher Lager, vor allem  in  der
Umgebung  von  Berlin.  Zunächst  geleitet  von  jüdischen  Vereinen,
wurden  diese  „Umschulungslager“  aber 1940 von NS-Organisationen
übernommen und umfunktioniert, sie wurden zu Zwangsarbeitslagern. 

In Schönfelde_ (Kreis Lebus bei Fürstenwalde/Spree) gab es ein solches
Forst-  und  Ernteeinsatzlager, und  Käthe Orenstein ist dort gewesen.
Auch das ist den wenigen Dokumenten, die wir von Käthe  haben,  zu
entnehmen.  Diese  Lager wurden  schließlich  aufgelöst,  Schönfelde im
April 1943, und die Behörden haben die Lagerinsassen deportiert und
praktisch durchweg ermordet. Das ist wohl Käthes trauriges Schicksal
gewesen.  Ihre Schwestern, die die Verfolgungen überlebt haben, konnten
sie nur noch für tot erklären zu lassen.

.. _Schönfelde: http://de.wikipedia.org/wiki/Sch%C3%B6nfelde

`Mehr zu Familie Orenstein <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2021/2021-fam-orenstein.pdf>`_

.. _Heinrich Orenstein: orenstein-he.html
.. _Selma: orenstein-se.html
.. _Zita: orenstein-sa.html
.. _Fay: orenstein-fa.html
.. _Käthe: orenstein-ka.html
.. _Cilly: orenstein-ci.html
.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Noé: http://de.wikipedia.org/wiki/No%C3%A9%20%28Haute-Garonne%29
.. _Récébédou: http://de.wikipedia.org/wiki/Portet-sur-Garonne
.. _Wiederherstellung des Berufsbeamtentums: http://de.wikipedia.org/wiki/Gesetz%20zur%20Wiederherstellung%20des%20Berufsbeamtentums
.. _Hermann Maas: http://de.wikipedia.org/wiki/Hermann%20Maas%20%28Theologe%29
.. _Polenaktion: http://de.wikipedia.org/wiki/Polenaktion
.. _Nürnberger Gesetze: https://de.wikipedia.org/wiki/N%C3%BCrnberger_Gesetze
