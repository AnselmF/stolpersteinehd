Sigmund Beer
============

.. container:: dates

  -REPLACE-WITH-DATES-

Sigmund Beer wurde 24. August 1886 in Baiertal als als Sohn von Bernhard
Beer und Flora Seligmann geboren.  Sein Vater war dort Bäcker.  Ob
Sigmund im elterlichen Betrieb das Bäckerhandwerk gelernt hat, wissen
wir nicht.

1919 heiratete er `Bertha Hochstädter`_ und zog mit ihr nach Rohrbach,
das damals noch ein unabhängiges Dorf war.  Dort kaufte 
seine Frau das Haus in der Rathausstraße 64, in dem zuvor
Nathan Gutmann eine kleine
Teigwarenfabrik betrieben hatte.  Das Ehepaar führte diese als „Nudel- und
Mazzenfabrikation“ fort.  Bald waren sie unter dem Namen „Nudelbeer“
in Rohrbach ein Begriff.  Die Familie wohnte im ersten Stock des Hauses,
ein weiteres Stockwerk hatten sie vermietet.

Am 1. September 1920 brachte Sigmunds Frau das einzige Kind der Famile,
`Ernst Berthold`_, zur Welt.

Während der Weltwirtschaftskrise Ende der 1920er Jahre ging es der
Kleinunternehmerfamilie Beer finanziell nicht gut.  Sigmund Beer war
überschuldet und konnte seinen Verpflichtungen nicht nachkommen. 

Mit der Machtübertragung an die Nationalsozialisten Anfang 1933 kam auch
die Beer'sche Nudelfabrikation auf die Liste der zu boykottierenden
Geschäfte. Somit war ein Ende der finanziellen Not erst recht in weite
Ferne gerückt. Mehr schlecht als recht konnte das Paar das Geschäft
dennoch bis zur Pogromnacht vom 9. auf den 10. November 1938 fortführen.

Dennoch war ihnen die Gefahr offenbar bewusst, denn sie bemühten sich,
ihren Sohn in Sicherheit zu bringen; im September 1938 konnte dieser zu
Verwandten in die USA ausreisen.

Während der Pogromnacht verwüsteten Mitglieder des
SA-Studentensturms und des Pioniersturms der SA zunächst die schräg
gegenüber dem Beer'schen Haus gelegene Rohrbacher Synagoge und setzten
ihr Zerstörungswerk im Haus der Beers fort, als diese brannte.  Noch
lange erinnerten sich Zeugen an die auf der Straße liegenden
ausgeleerten Mehlsäcke.

Am darauffolgenden Tag deportierte reguläre Polizei Sigmund Beer wie
weitere 74 Juden aus Heidelberg in das Konzentrationslager Dachau.  

Am 22. Dezember 1938 kehrte er nach Heidelberg zurück.  Sein Unternehmen
wird es zu diesem Zeitpunkt nicht mehr gegeben haben, denn spätestens am
23. November 1938 wurden die noch bestehenden Heidelberger
Einzelhandels- und Handwerksbetriebe „arisiert“.  Dennoch wird Sigmund
Beer noch im Heidelberger Adressbuch für 1940 für die Rathausstraße 64
mit dem diskriminierenden Namenszusatz „Israel“ als Eigentümer geführt.

Unterdessen konnte seine Frau nicht verhindern, dass ihr Haus im März
1940 (an den Kreisobersekretär Ludwig Reinhardt) zwangsversteigert
wurde.
Spätestens ab diesem Zeitpunkt bewohnte das Ehepaar im „Judenhaus“ am
Marktplatz 7 in Heidelberg ein Zimmer.  Von dort haben die Behörden sie
und die anderen acht jüdischen BewohnerInnen des Hauses im Rahmen der
`Wagner-Bürckel-Aktion`_ vom 22.10.1940 in das Konzentrationslager Gurs_
verschleppt.

In einem mit Zeltplanen überdeckten Lastwagen wurden sie zum Hauptbahnhof, damals noch gegenüber dem heutigen Adenauerplatz, transportiert.  Mehrere 
ZeugInnen berichten später in einem Spruchkammerverfahren von einem
Vorfall, der sich kurz vor der Abfahrt auf dem Marktplatz ereignet
hatte: „Beim Anfahren des Wagens kam aus der Menge ein Soldat auf den
Wagen zu und schlug dem am Ende des Wagens rechts sitzenden Juden
[Sigmund] Beer (Nudelfabrikant) eine ins Gesicht", da dieser sich beim
Besteigen des Lastwagens „durch Zuruf zu einer Person verabschiedete“.

Sigmund Beer und seine Frau überlebten in Gurs zwei Winter, während ihr
Sohn sich von den USA aus bemühte, ihnen die Flucht zu ermöglichen.
Seit 1939 ohne jeglichen Besitz, waren die Eltern womöglich
nicht mehr in der Lage, ihre Ausreise samt Reichsfluchtsteuer zu
finanzieren.  Die Ausreisepapiere, die Berthas Sohn zum amerikanischen
Konsulat nach Stuttgart geschickt hatte, blieben ungenutzt.  Auch der
Versuch, sie aus Gurs zu befreien, misslang, die nach Marseille
geschickten Papiere erreichten die Beers nicht mehr. 

Am 10. August 1942 wurde Sigmund Beer, mittlerweile ins Lager Drancy in
Frankreich deportiert, gemeinsam mit seiner Frau nach Auschwitz
verschleppt und dort vermutlich gleich nach seiner Ankunft ermordet. 

`Mehr zu Familie Beer <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2016/2016_Beer.pdf>`_

.. _Bertha Hochstädter: beer-be.html
.. _Ernst Berthold: beer-eb.html

.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
