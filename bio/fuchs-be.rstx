Dr. Berthold Fuchs
==================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: fuchs-be.jpeg

   Berthold Fuchs (Rechte: `Alex Calzareth`_)

.. _Alex Calzareth: http://calzareth.com/

Berthold Fuchs wurde am 30. April 1890 in Odenheim bei Bruchsal geboren.
Sein Vater war der Arzt Dr. Nathan Fuchs, seine Mutter Babette Fuchs,
geborene Bär. Wie schon sein Vater und sein älterer Bruder Julius
studierte Berthold Fuchs Medizin, ab 1908 war er an der Universität
Heidelberg eingeschrieben.

Während seines Studiums wohnte Fuchs in verschiedenen Studentenzimmern
in Bergheim und der Weststadt. Im Ersten Weltkrieg war er als
Assistenzarzt der Reserve im Einsatz. Im September 1915 baten Berthold
Fuchs und seine Verlobte Alice Eisemann um Befreiung vom Eheaufgebot, da
sich Fuchs weit weg „im Felde“ befinde.  

Alice Eisemann war die am 2.  April 1891 in Heidelberg geborene Tochter
von Max_ und Johanna Eisemann, geb. Weil, und lebte zu diesem Zeitpunkt
noch bei ihren Eltern in der Bismarckstraße. Der im Juni 1917
verstorbene Großvater von Alice, Leon Weil, hatte als eine seiner
letzten Eintragungen in seinem Notizheft noch „Oberstabsarzt Dr.  Fuchs,
im Felde“ aufgeführt.

Ab Mai 1919 war Fuchs für die folgenden Jahre in der Bunsenstraße 3
gemeldet, unterbrochen nur von einem Aufenthalt in Erlangen im Jahr
1923. Am 1.  Mai 1924 eröffnete Dr. med. Berthold Fuchs in der
Bunsenstraße 3 seine Praxis als Facharzt für Innere Medizin. Soweit
bekannt ist, engagierte sich Dr. Fuchs nicht in der jüdischen Gemeinde,
zumindest ist er in keinem der Gremien, Vereine oder im Vorstand
vertreten oder als Förderer erwähnt.

Im Jahr 1933 zog er mit seiner Frau Alice und seinem seit Anfang des
Jahres verwitweten Schwiegervater Max Eisemann in die Blumenstraße 15,
wo er die Praxis von Dr. med. Karl Windel
übernahm. Die riesige Wohnung mit acht Zimmern auf 230 qm reichte gut
für die Praxis und die BewohnerInnen.  Sie war im reinen Jugendstil
mit hochwertigen Materialien gebaut, repräsentativ ausgestattet und auf
dem neuesten Stand, sogar mit zentraler Staubsauganlage. Im Haus wohnten
auch die Miteigentümer Meta und Julius Koppel. Meta war eine geborene
Fuchs, Schwester von Berthold Fuchs.

Seit April 1933 waren jüdische Bürger und Firmen vom Boykottaufruf der
NSDAP betroffen. Ab 1936 führte das Heidelberger Adressbuch unter der
Ärzte-Rubrik gesondert die „nicht-arischen“ Ärzte auf.  Dr. Fuchs war
einer der 17 dort genannten Mediziner.  Deren Zahl wurde von Jahr zu
Jahr geringer. „Arische“ Patienten trauten sich immer weniger zu
jüdischen Ärzten, und die Zahl der jüdischen Patienten wurde durch die
Flucht vieler ins Exil ebenfalls geringer.  

Die Jahre 1937/38 brachten einschneidende Veränderungen für die
Lebenssituation von Dr. Fuchs. Am 21. November 1937 verstarb seine
Ehefrau Alice nach schwerer Krankheit.  Die Ausgrenzungs- und
Verfolgungsmaßnahmen verschärften sich immer weiter. 1938 wurde per
Reichsgesetz jüdischen Ärzten die Approbation entzogen. Berthold Fuchs
war wohl der einzige jüdische Mediziner in Heidelberg, der mit
Genehmigung noch als „Krankenbehandler“ für ausschließlich jüdische
Patienten arbeiten durfte.
Außerdem musste Dr. Berthold Fuchs wie andere seine
Wohnung verlassen. Er konnte einige Häuser weiter, in der Häusserstraße
20 noch für wenige Monate wohnen und praktizieren.

Am Morgen nach der Reichspogromnacht vom 9./10. November 1938, früher
verharmlosend „Reichskristallnacht“ genannt, haben SA-Angehörige 
seine Praxis von verwüstet. Es gibt einen Zeitzeugenbericht von Heiner
Markmann, der als 12-jähriger mit dem Fahrrad auf dem Weg von
Handschuhsheim zum Hans-Hassemer-Sportplatz in der Weststadt unterwegs
war. Als er durch die Häusserstraße radelte, sah und hörte er, wie Randalierer
vom Balkon „eines der schönen Gebäude“ den Instrumentenschrank 
aus der Praxis in den Vorgarten hinabwarfen.
„Ich höre dieses Scheppern und schreckliche Klirren
heute noch“. Das fürchterliche Geräusch blieb Markmann unvergesslich,
auch wenn der Junge damals die Tragweite der Ereignisse noch nicht
begreifen konnte.

Berthold Israel Fuchs, wie er sich nun nennen musste, zog Anfang 1939
nach Mannheim. Einen Großteil seiner Wertsachen hatte er noch weit unter
Wert im Leihhaus Heidelberg zur Versteigerung abgeben müssen.  Fuchs gab
seine Zulassung als „Krankenbehandler“ zurück, ging mit der 1913 in
Mannheim geborenen Lotte Melanie Marx seine zweite Ehe ein und konnte
Anfang 1940 gerade noch aus Deutschland fliehen.  Die USA nahmen das
Ehepaar Fuchs auf. 1943 eröffnete Dr. Bert Fuchs seine Praxis in New
York, die er bis 1952 betrieb.

Am 2. August 1954 starb Dr. Fuchs im Alter von 64 Jahren in New York.
Die Todesmeldung erschien in der Zeitschrift „Aufbau“ am 13. August
1954.

`Quelle <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2020/25_Fuchs.pdf>`_

.. _Max: eisenmann-ma.html
