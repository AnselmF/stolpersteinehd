Dr. Isaak Sussmanowitz
======================

.. container:: dates

  -REPLACE-WITH-DATES-

Isaak Sussmanowitz wurde 1870 im litauischen Garsden geboren. Sein
Geburtsort gehörte damals zum russischen Staatsgebiet. Viele
deutschsprachige Juden lebten dort im Vergleich zu ihren russischen
Glaubensbrüdern zunächst unangefochten und hatten ihre Talmudschulen, in
denen sie Rabbiner für ganz Russland ausbildeten. Erst 71 Jahre später
sollte die bis dahin unbekannte Kleinstadt Garsden traurige Berühmtheit
erlangen: Hier fand 1941 das erste Massaker deutscher Polizeieinheiten
`auf dem Boden der Sowjetunion`_ statt.

.. _auf dem Boden der Sowjetunion: https://de.wikipedia.org/wiki/Garsden#Geschichte

Isaak hatte sich für den Arztberuf entschieden, aber als deutscher Arzt
jüdischen Glaubens war er schon damals in Russland Schikanen ausgesetzt.
Im deutschen Kaiserreich glaubte er sicher zu sein, auch wenn der
häufige Ortswechsel der folgenden Jahre ahnen lässt, dass auch dort die
Gleichstellung nicht erreicht war.  

1900 kam er zunächst nach München, von dort nach Beerfelden im Odenwald,
wo er kurz als Gemeindearzt tätig war, von dort im Oktober 1902 in das
Dorf Zeiskam in der Pfalz. Dort praktizierte er elf Jahre lang und
lernte seine spätere Frau `Laura Metzger`_ kennen.  Die beiden
heirateten 1906 in Schwetzingen.  Noch in Zeiskam kamen 1908 der Sohn
`Ernst Max`_ und 1909 die Tochter `Edith Sophie`_ zur Welt.

Im September 1913 ergab sich für Dr. Sussmanowitz die Gelegenheit, in
Speyer die freiwerdende Arztstelle eines jüdischen Kollegen zu
übernehmen. Es ist anzunehmen, dass er in der Pfalz mit ihrer alten
jüdischen Kultur und zumal in der städtischen Atmosphäre Speyers hoffte,
mit seiner Familie gut aufgehoben zu sein. Seine Tochter Edith
allerdings erinnerte sich noch als Hundertjährige im `Gespräch mit Ria
Krampitz`_ schmerzhaft daran, dass bei ihnen zu Hause wenig gelacht
wurde. 

Zeitzeugen schildern den Doktor als einen stattlichen Herrn, der aus
alter medizinischer Kenntnis seiner baltischen Heimat heraus gerne mit
Heilkräutern arbeitete und sehr sozial eingestellt gewesen sei.  Als
„Arbeiterarzt“ sei er bekannt gewesen und kämpfte, wie viele Ärzte
damals, gegen die grassierende Tuberkulose und Säuglingssterblichkeit.
Während des Ersten Weltkriegs arbeitete Isaak Sussmanowitz in den
Speyerer Militärlazaretten und erhielt dafür 1916 vom Bayerischen Staat
das König-Ludwig-Kreuz auf Kriegsdauer.

Im Jahr 1927 ehrte ihn die Stadt Speyer mit dem Titel „Sanitätsrat“.
Jedoch erlitt er schon zwei Jahre später im Alter von 59 Jahren einen
Herzinfarkt, der ihn zwang, seine Praxis einem Nachfolger zu übergeben.

Inzwischen hatten Sohn und Tochter ihr Abitur bestanden und in
Heidelberg das Medizinstudium begonnen. So zogen die Eltern 1929
zusammen mit den Kindern zunächst in eine geräumige Wohnung in der
Heidelberger Weststadt, Zähringer Straße 8, wo sie in den Heidelberger
Adressbüchern bis 1930 bezeugt sind. Aber bereits damals begegneten
Juden in der Stadt und der Universität zunehmender Feindseligkeit.

Kurz nach der Machtübergabe an die NSDAP 1933 flohen die beiden
Kinder des Ehepaars Sussmanowitz über die Niederlande in die
Sowjetunion.  Dort ermordete der stalinistische Apparat während der
Säuberungen von 1938 Isaaks Sohn Ernst Max wegen dessen kommunistischer
Gesinnung.  Tochter Edith und deren Mann konnten weiter nach Schweden
fliehen.
 
Nach der Ausreise der Kinder zog das Ehepaar zunächst für zwei Jahre in
die Leopoldstraße 53 b (heute: Friedrich-Ebert-Anlage), vermutlich in
eine kleinere, preiswertere Wohnung.  Zwischen 1935 und 1938 lebten sie
schließlich in der Goethestraße 12, dem letzten freiwillig gewählten
Domizil der beiden.  Deshalb sind dort auch die Stolpersteine für Isaak
Sussmanowitz und seine Familie verlegt.

Im November 1938 brannten_ in Heidelberg und anderswo die Synagogen. Wer
jüdischer Herkunft war, musste im folgenden Jahr in 
der Regel Geschäft, Haus oder Wohnung
verlassen. Die Weberstraße 5 in Neuenheim war als eins der „Judenhäuser_“
Heidelbergs 1939 die neue Zwangsadresse des Paares.  Von dort haben die
deutschen Behörden Isaak Sussmanowitz und seine Frau am 22.10.1940
im Rahmen der `Wagner-Bürckel-Aktion`_ nach Frankreich deportiert, wo
sie im Internierungslager Gurs_ eingesperrt wurden. 

.. _Judenhäuser: http://de.wikipedia.org/wiki/Judenhaus
.. _brannten: http://de.wikipedia.org/wiki/Novemberpogrom%201938

Die schrecklichen Zustände im Lager, vor allem im kalten, regennassen
Winter, sind hinlänglich bekannt. Hier verstarb Issak Sussmanowitz einen
Monat nach der Ankunft elendig, auf blankem Boden liegend, an seinem 70.
Geburtstag.

`Mehr zu Isaak Sussmanowitz und seiner Familie <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2014/2014_Familie_Sussmanowitz_Szekely.pdf>`_

.. _Gespräch mit Ria Krampitz: https://www.speyer.de/de/familie-und-soziales/senioren/demografische-entwicklung/2012.2-interview-dr.-edith-szekely.pdf
.. _ernst max: sussmanowitz-em.html
.. _edith sophie: sussmanowitz-ed.html
.. _Laura Metzger: sussmanowitz-la.html
.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
