Luise Margarete Bornstein, geb. Schütze
=======================================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: bornstein-lm-hb.jpeg

   Luise Margarete Bornstein (rechts) mit ihrem Mann und einem ihrer
   Kinder (Foto: Fam. Rosenblatt).

Luise Margarete Schütze wurde am 5. August 1903 als Tochter von August
und Katharina Schütze in Heidelberg geboren.

Am 15.10.1929 heiratete sie `Hugo Bornstein`_.  Sie brachte 1931
Werner_, 1934 `Hans Eduard`_,  1937 `Margarete Luise`_ und 1938 `Inge
Ruth`_ Bornstein zur Welt.  

Unterdessen nahm unter der NS-Regierung der Druck auf ihren jüdischen
Mann und hier als „Halbjuden“ klassifizierten Kinder immer weiter zu.
Selbst ihre Angehörigen hielten sich aus Sorge um die Folgen eines
Umgangs mit Juden von ihr fern.  1935 kündigte die Stadt ihrem Mann in
Umsetzung der rassistischen Nürnberger Gesetze.
Dieser wurde fortan von seinem Freund Fritz Trefz, der beim Wehrmeldeamt
Heidelberg arbeitete, zumeist außerhalb von Heidelberg
für alle möglichen Jobs eingesetzt, während des
zweiten Weltkriegs oft in den besetzten Gebieten.  

1943 wurde die Situation in der Stadt so untragbar, dass sich Luise Bornstein
mit ihren Kindern in einem kleinen Gartenhaus auf einem Grundstück im
Wald oberhalb des oberen Gaisbergwegs versteckte.  Dort wurden sie von
FreundInnen der Familie heimlich mit Lebensmitteln versorgt, allen voran
von den Angehörigen von Fritz Trefz.
Bis zur Befreiung hat niemand dieses Versteck verraten.

1945 kehrte die Familie in die Hauptstraße 111 zurück 
und wohnte anschließend
für eine Weile in der Handschuhsheimer Landstraße 8, während Luises Mann
für die US-Behörden arbeitete.  1948 bot sich die Gelegenheit, ins
damals noch britische Mandatsgebiet Palästina auszuwandern.  Zunächst
wurden dabei die Kinder ausgeflogen und in verschiedenen Kibbuzim
untergebracht, am 15. Oktober 1949 folgten auch die Eltern per Schiff.
Die Eltern versammelten die Kinder wieder, als sie zunächst im Jordantal
versuchten, sich mit Landwirtschaft über Wasser zu halten.  Später zog
die Familie nach `Naharija`_, einer Stadt am Mittelmeer zwischen Tel
Aviv und der libanesischen Grenze.

Luise Margarete Bornstein, die inzwischen zum jüdischen Glauben
übergetreten war, starb dort am 22. Mai 1960.

`Mehr zu Familie Bornstein <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2020/43_Bornstein_-_Kopie.pdf>`_

.. _Hugo Bornstein: bornstein-hb.html
.. _Inge Ruth: bornstein-ir.html
.. _Margarete Luise: bornstein-ml.html
.. _Luise Margarete Schütze: bornstein-lm.html
.. _Werner: bornstein-we.html
.. _Hans Eduard: bornstein-he.html
.. _Naharija: http://de.wikipedia.org/wiki/Naharija
