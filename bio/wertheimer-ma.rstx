Max Wertheimer
==============

.. container:: dates

  -REPLACE-WITH-DATES-

Max Wertheimer wurde am 31. März 1888 in Östringen geboren.  Er hatte
einen älteren Bruder, Julius_. Über die Eltern von der beiden wissen wir
nichts.


Er erlernte den Kaufmannsberuf und heiratete am 22. November 1920 `Rosa
Strauß`_.  Mit ihr zog er 1922 nach Heidelberg.  Das Paar wohnte
zunächst in Neuenheim (Mönchhofstraße 30, Lutherstraße 22).  Mit seinem
Bruder Julius gründete er die Gebrüder Wertheimer Handelsgesellschaft,
die 1929 als Firma wieder gelöscht wurde.

Wovon die Wertheimers danach lebten, ist nicht bekannt.  Ihr letzter
frei gewählter Wohnort war seit 1934 die Bluntschlistraße 4.  Der
polnisch-jüdische Eigent+mer `Isak Engelberg`_, der das Haus selbst
erst kurz zuvor, 1932, von den Erben der Zigarrenfabrikanten Max und
Ferdinand Liebhold erworben hatte, wurde im Oktober 1938 Opfer der
„Polenaktion_“.

Diese „Polenaktion“ brachte Herschel Grynszpan dazu, einen Mitarbeiter
der deutschen Botschaft in Paris zu erschießen, was der deutschen
Regierung wiederum zum Vorwand diente, die Pogromnacht_ vom 9. November
1938 zu inszenieren.  In deren Folge haben die Behörden Max Wertheimer
zusammen mit 75 weiteren jüdischen Heidelberger Männern (reichsweit
26000) in das `Konzentrationslager Dachau`_ verschleppt, was euphemistisch
als „Schutzhaft“ bezeichnet wurde.  Bis zum 15. Dezember 1938 musste er
im völlig überfüllten Lager bleiben und wurde u.a. durch ständige
Appelle gequält.

.. _Pogromnacht: http://de.wikipedia.org/wiki/Novemberpogrom%201938
.. _Konzentrationslager Dachau: http://de.wikipedia.org/wiki/KZ%20Dachau

Nach der Abschiebung des Eigentümers nutzten die Behörden die
Bluntschlistraße 4 als „Judenhaus_“, also zur Konzentration von JüdInnen
vor der Deportation.  Insgesamt 14 Personen versammelten sie dort, bis
sie am 22. Oktober 1940 in der `Wagner-Bürckel-Aktion`_ fast alle
JüdInnen aus Baden und der Saarpfalz nach Vichy-Frankreich und damit ins
Lager Gurs_ deportierten.

.. _Judenhaus: http://de.wikipedia.org/wiki/Judenhaus

Auch Max Wertheimers Bruder Julius und dessen 
Frau Klara waren im Transport.  Beide
fielen bereits im ersten Winter den fürchterlichen Zuständen im Lager
zum Opfer.  Max und seine Frau überlebten.  Am 14. August 1942 jedoch
schafften die deutschen Behörden die beiden in das Durchgangslager
Drancy.  Zwar verlieren sich dort die Spuren der Wertheimers, doch ist
praktisch sicher, dass die Behörden sie in einen der Todeszüge nach
Auschwitz zwangen und – da die Wertheimers in ihrem 
Alter und nach den Torturen von Gurs nicht 
mehr als arbeitsfähig klassfifiziert worden sein werden – dort
unmittelbar haben ermorden lassen.

`Mehr zu Max und Rosa Wertheimer <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2015/Max_Rose_Wertheimer.pdf>`_

.. _Julius: wertheimer-j.html
.. _Rosa Strauß: wertheimer-ro.html
.. _Isak Engelberg: engelberg-i.html
.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Noé: http://de.wikipedia.org/wiki/No%C3%A9%20%28Haute-Garonne%29
.. _Récébédou: http://de.wikipedia.org/wiki/Portet-sur-Garonne
.. _Wiederherstellung des Berufsbeamtentums: http://de.wikipedia.org/wiki/Gesetz%20zur%20Wiederherstellung%20des%20Berufsbeamtentums
.. _Hermann Maas: http://de.wikipedia.org/wiki/Hermann%20Maas%20%28Theologe%29
.. _Polenaktion: http://de.wikipedia.org/wiki/Polenaktion

