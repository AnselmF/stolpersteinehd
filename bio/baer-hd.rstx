Hans Dieter Baer
================

.. container:: dates

  -REPLACE-WITH-DATES-

Hans Dieter Baer wurde am 20. Oktober 1926 als Sohn des Mannheimer
Landgerichtsrats `Alfred Baer`_ und dessen Frau `Klara`_ geboren.  Er
hatte eine drei Jahre ältere Schwester, `Doris Ellen`_

.. _Alfred Baer: baer-al.html
.. _Klara: baer-kl.html
.. _Doris Ellen: baer-de.html

Nachdem die NSDAP-Regierung seinen Vater mit den `Nürnberger Gesetzen`_
aus dem Staatsdienst entfernt hatte, zog die Familie 1935 in das Haus in
der Dantestraße (damals: Kronprinzenstraße) 24.

.. _Nürnberger Gesetzen: http://de.wikipedia.org/wiki/N%C3%BCrnberger%20Gesetze

Hans Dieter Baer besuchte bis November 1938 das (seit 1937 so heißende)
Kurfürst-Friedrich-Gymnasium.  Er wurde im Gefolge der Pogrome vom 9.
November auf Weisung_ des Reichserziehungsministers Rust vom 15.
November 1938 von der Schule ausgeschlossen, da

.. pull-quote::

  keinem deutschen Lehrer und keiner deutschen Lehrerin mehr zugemutet
  werden [kann], an jüdische Kinder Unterricht zu erteilen […] Auch
  versteht es sich von selbst, daß es für deutsche Schüler und
  Schülerinnen unerträglich ist, mit Juden in einem Klassenraum zu sitzen.

.. _Weisung: http://digitalpast.de/2013/11/15/weisung-des-reichserziehungsministers-rust-vom-15-november-1938/

Seine Eltern konnten ihn im März 1939 mit einem der Kindertransporte_
nach England retten.  1940 deportierten die deutschen Behörden seine
Eltern ins Lager Gurs.  Vom Tod seines Vaters im Lager Récébédou erfuhr
sie noch aus einem Brief seiner Mutter, die Ermordung der Mutter in
Auschwitz im August 1942 wurde erst nach dem Krieg Gewissheit.

.. _Kindertransporte: http://de.wikipedia.org/wiki/Kindertransport

Hans Dieter Baer hat in England weiter die Schule besucht, unter anderem
das bereits 1584 gegründete Internat `Uppingham School`_, trat dann in
die britische Armee ein und studierte anschließend Naturwissenschaften
in Cambridge.  Er starb am 22. März 2017 

.. _Uppingham School: https://en.wikipedia.org/wiki/Uppingham_School

`Mehr zu Familie Baer <http://stolpersteine-heidelberg.de/familie-baer.html>`_
