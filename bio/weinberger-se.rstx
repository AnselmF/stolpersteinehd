Selma Weinberger, geb. Kaufmann
===============================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: weinberger-se.jpeg

   Selma Weinberger (Leo Baeck Institut, DigiBaeck_ AR 25541)

Selma Weinberger wurde am 17. April 1889 in Schriesheim als Tochter des
Rohtabakhändlers Ferdinand Kaufmann und dessen  Ehefrau  Jeanette (geb.
Feist) geboren.

Am 19. März 1914 heiratete sie den in Rastatt praktizierenden Arzt
`Sigmund Weinberger`_, der ab August 1914 als Stabsarzt an der Westfront
des ersten Weltkriegs arbeitete.  Nach seiner Rückkehr zog das Paar in
die Rohrbacher Straße 43, wo Selmas Mann auch praktizierte.  In dem
großen Haus wohnten auch ihre Eltern.  Ihr Vater starb im Juni 1933 im
Alter von fast 90 Jahren.

Selma brachte zwei Kinder zur Welt, Bertha und Benjamin.

Zunächst mit der Wirtschaftskrise, verstärkt infolge des nach der
Machtübergabe an die NSDAP dramatisch wachsenden Antisemitismus, ging
die wirtschaftliche Basis des Paares im Laufe der 1930er Jahre immer
weiter verloren.  Schließlich flohen die Weinbergers im Januar 1937 über
Hamburg in die USA.

Sie kamen am 25. Januar 1937 in New York an.  Bei der Passkontrolle
erlitt Selma Weinberger eine Herzembolie und starb noch während der
Einreiseformalitäten.

.. _Sigmund Weinberger: weinberger-si.html
.. _DigiBaeck: https://www.lbi.org/collections/digibaeck/

`Mehr zum Ehepaar Weinberger <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2020/27_Weinberger.pdf>`_
