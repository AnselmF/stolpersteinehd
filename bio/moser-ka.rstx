Karl Moser
==========

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: moser-ka.jpeg

   Karl Moser im August 1908, Portraitfoto aus der Heidelberger
   Krankenakte (01/199)

Karl Moser wurde am 10. Januar 1876 in Heidelberg als zweiter Sohn eines
Heidelberger Dekorationsmalers geboren.  Bereits  in seiner Schulzeit
hatte der Knabe ein  eigenes Laboratorium daheim. Nach der Realschule
nahm er 1894 ein Chemiestudium auf, besuchte erst das Polytechnikum in
Straßburg, dann die Universitäten  in Heidelberg und Gießen. 1896 setzte
er  das Studium wieder in Heidelberg fort.  Von den Sommerferien 1899 an
arbeitete er dann aber in der Darstellung seiner Krankenakte „weniger
und gleichgültiger“.

Moser hörte nun „religiöse Vorträge, wurde frömmelnd, sprach viel von
Gott und Religion“. Außerdem fing er an  zu trinken, und wenn er las, so
nur mehr „zur Unterhaltung moderne Schriftsteller“.  Im November 1900
brachte man ihn  schließlich in das private Kurhaus für Nerven- und
Gemütskranke Neckargemünd,  weil er „aufgeregt“ war, sich „ständig im
Wirtshaus“ aufhielt, „Uhr oder Kette“ verpfändete, wenn er kein Geld
hatte, und  schließlich sogar „drohend gegen seine  Angehörigen“ wurde,
die sich weigerten,  ihm Alkohol zu beschaffen. In der Anstalt erschien
er in Gehrock und Zylinder und verschickte in den ersten Tagen  eine
Vielzahl von Telegrammen, in denen  er nach Pistolen, einem Reitpferd,
seinem Leibburschen, einem Fahrrad und Besuch verlangte.

Die Ärzte verzeichneten  „acute Manie“.  Er  war aggressiv gegen die
Wärter, zerbrach  Dinge in seinem Zimmer und kam „daher ins Dauerbad“.
Aber schon im Februar 1901 fühlte sich Moser wieder „gut“  und war
„einsichtsvoll“, so dass er nach  Hause zurück konnte.

Friedlich und unauffällig verhielt er  sich für neun Monate. Später
einmal gab  er an, der November sei stets eine kritische Zeit für ihn.
Unstetem Lebenswandel, erhöhtem Alkoholkonsum und  Verschuldung folgte
im Dezember die  Einweisung in die psychiatrische Universitätsklinik
Heidelberg.  Über sein Verhalten in dieser Zeit berichtet die
Patientenakte:

.. pull-quote:
  Hat sich ein Bettuch um den Hals geknotet, versucht einen
  Strangulierten  darzustellen. Schreit dann: der große  Pan ist tot!
  April! April! Und fängt dann  an zu gackern, zu piepen und zu
  schnattern, wirft sein ganzes Bettzeug im Zimmer herum, amüsiert sich
  ofefnbar gut.  […] Auf Anrede reagiert er nur mit Fratzen, Gelächter
  und Unflätigkeiten die er  sehr geniesst.

Über den Januar 1902 hinweg beruhigte er sich offenbar.  Bevor er im
März auf Antrag seines Vaters entlassen wurde, notierte der behandelnde
Arzt:

.. pull-quote::
  Bemühte sich sichtlich allen Vorschriften nachzukommen: Hält auf
  Äusseres,  benutzte aber, da er kein Haaröl bekam,  zur Anfertigung
  seines Scheitels Bouillon  oder Speckschwarte. Kein Gefühl für die
  Schwere seiner Erkrankung. Will weiter  studieren, hat sich aber in
  keiner Weise  mit seinem Studium beschäftigt, sondern sich in seiner
  Lektüre auf Woche  u[nd] Lustige Blätter beschränkt. Lebhaft,
  euphorisch, unterhaltend. Eltern  behaupten er sei wie vor seiner
  Erkrankung.

In den folgenden Jahren arbeite er für seinen Vater, dann lernte er bei
einem Gartenbauinstitut in Weinheim und arbeitete als Gärtnergehilfe in
Wieblingen, später an verschiedenen Stellen in  Frankfurt am Main. Nach
dem Tod des  Vaters Ende 1904 kam er nach Heidelberg zurück und
arbeitete im väterlichen  Geschäft mit.

Erneut wuchs die Unzufriedenheit mit seiner Lebenssituation.  Diesmal
hieß der Fluchtpunkt Amerika.  Die Verwandten, bei denen er wohnte,
schickten ihn jedoch nach acht Monaten  zurück, da „seine Mittel zu Ende
waren“  und er „etwas aufgeregt war und bei jeder Arbeit sehr leicht
müde“. Nach einem  Jahr verließ er Heidelberg erneut und  ging nach
Erfurt, wo er „einige Male die  Stellung und den Beruf“ wechselte. Dabei
gab er zu Protokoll: „Arbeitsscheu ist er  nie gewesen, im Gegenteil er
war unternehmungslustig, bloss fehlte ihm stets  die Arbeitsausdauer.“

Er kehrte 1908 nach Heidelberg zurück, landete wieder in der
Psychiatrie.  Die Diagnose lautete nun: „Manischdepressiv“. Doch bald
schon arbeitete  Moser im Garten der Klinik, und Mitte  Dezember wurde
er entlassen.

Für die Zeit des  Ersten Weltkriegs verzeichnet die Krankenakte eine
Gärtnerlehre – vermutlich  eine späte Fortsetzung der in Weinheim
begonnenen Ausbildung. 1922 war Moser erneut in Heidelberg, ohne
„geregelte Tätigkeit“, „zeitweise“ von Fürsorgeunterstützung lebend.

Für die Jahre er Weimarer Republik gibt es kaum Informationen.  1925
hat er von sich aus die Heidelberger Klinik aufgesucht, Diagnose dann:
„Schizoider, Paraphrenie“. Aufgenommen  wurde er nicht.  1932 wies man
behördlich einen Antrag auf Entmündigung wegen Alkoholismus zurück,

1936 wurde von der  Heidelberger Klinik „erneut
Aufnahme  erwogen“. Zur Einweisung kam es aber  erst wieder im September
1940, als Mosers Verhalten im Krieg bedrohlich für  seine Umgebung
wurde. Diesmal brachte ihn ein Kriminalbeamter „gegen seinen  Willen“.
Er habe sich „in sexuell anstößiger Weise“ einer Untermieterin genähert,
vor allem aber „beachtete er die Verdunkelungsmassnahmen nicht, krakelte
u.  schimpfte im Keller“.

Bei der Aufnahme  wurde festgehalten: „Pat.  fällt durch sein  lautes,
rechthaberisches Wesen auf, ist  völlig uneinsichtig, eine Besprechung
mit  ihm ist gar nicht möglich“.  Klassifiziert  wurde er nun als
unbehandelbar: „Schizophrener Defektzustand“.

Deshalb verlegte man Moser schon am nächsten Tag  in die
Heil- und Pflegeanstalt Wiesloch.  Im Oktober 1943 kam er wegen
kriegsbedingter Veränderungen der Anstalt  nach Emmendingen, von dort im
Januar  1945 aus ähnlichen Gründen nach Kaufbeuren. In dieser Anstalt
starb er am 2. Mai 1945 an einer Hand\ phlegmone_, einer  typischen Folge
langen Hungerns.

.. figure:: moser-lieder.jpeg

  Karl Moser, „Leid- und Liebeslieder“, 1902,  Bleistift auf
  Schreibpapier, 10,9 x 7,0 cm,  Sammlung Prinzhorn, Inv.Nr. 1408

In der Heidelberger `Sammlung Prinzhorn`_ haben sich von Moser einige
kleine Zeichnungen von 1902 erhalten, aber  auch Briefe und
Briefentwürfe von 1902  und 1908 sowie ein „Allerleiheft“ von  1908.  Er
schreibt mal empört, mal  bittend an seine Eltern, Geschwister und
andere Verwandte, aber auch an die behandelnden Ärzte, fordert Besuch,
Versorgung mit Kleidung und anderem sowie seine Entlassung.

Daneben setzt er  immer wieder zu Schilderungen seines
Lebens an, die jedoch alle nach wenigen Sätzen abbrechen. Der Konflikt
mit  dem Vater, von dem sich Moser nicht anerkannt und gegenüber seinem
älteren  Bruder zurückgesetzt fühlte, schlägt wiederholt durch. Er ist
auch in den Zeichnungen präsent, nur auf andere Weise. Denn hier geht es
immer wieder um  Selbstdarstellungen Mosers und um deren aufwertende
Ausschmückung, etwa  in dem Entwurf des Titelbildes zu einer  Ausgabe
von Liedern: „Leid- und Liebeslieder eines Unstäten, Herausgegeben  und
gemacht von einem Freund der Frauen und Backfische“.  Im Zentrum  steht
eine Männerfigur, elegant gekleidet  in Anzug, Mantel und Hut, über der
eine  Krone schwebt. Ein vielfaches „Karl Moser“ fasst die Gestalt ein.
Angedeutete  Vorhänge, mit Blüten gerafft, geben eine  festliche
Umrahmung im bürgerlichen  Geschmack der Zeit.

.. _phlegmone: https://de.wikipedia.org/wiki/Phlegmone
.. _Sammlung Prinzhorn: https://de.wikipedia.org/wiki/Sammlung%20Prinzhorn


Mehr Informationen zu Karl Moser und
weitere Beispiele seiner Kunst finden sich in der
`Langfassung dieses Textes
<http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2023/04_Moser_Stolpersteine_2022_Finale_Datei-5.pdf>`_.
