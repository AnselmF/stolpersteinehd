Franz Oppenheimer
=================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: oppenheimer-fr.jpeg

  Franz Oppenheimer

Franz Martin Siegfried Oppenheimer wurde am 27. Februar 1921 in
Heidelberg als Sohn von Flora_ und `Saly Oppenheimer`_ geboren.  Sein
Vater arbeitete zu dem Zeitpunkt noch als Kaufmann in der Dossenheimer
Mehl-, Getreide- und Futtermittelhandlung seiner Eltern; ab Mitte der
1920er Jahre war er dann leitender Mitarbeiter der Sinsheimer
Hadernsortierfirma von Franz' Großvater.  Franz wuchs mit seiner kleinen
Schwester Eleonore_ in der Weststadt auf, zuerst in der Zähringerstraße
25, nach Eleonores Geburt 1925 dann in der Zähringerstraße 3a.

Franz besuchte die Pestalozzischule (Volksschule) unweit der Wohnung in
der Weststadt bis 1931.  Im April 1931 meldeten ihn die Eltern der
Familientradition folgend an der Oberrealschule mit Realgymnasium in der
Kettengasse, dem heutigen Helmholtz-Gymnasium, an.  Er hatte eine
behütete Kindheit.

Ein großer Einschnitt in seinem Leben war der 21. Dezember 1935.  Da
seine Eltern im Deutschland der NSDAP keine Zukunft für ihre Kinder
sahen, meldeten sie ihn in der Oberrealschule ab.  Im Alter von 14
Jahren schickten sie ihn in ein Internat in Florenz. Es handelte sich um
ein Landschulheim, das für Flüchtlingskinder aller Konfessionen aus
Deutschland eingerichtet wurde. Dort unterrichteten hochqualifizierte
Lehrkräfte, die sich ebenfalls im Exil befanden.

Wir können nur vermuten, wie Franz mit der neuen Situation zurecht kam.
Leider ist kein Briefwechsel zwischen Franz und seiner Famiie erhalten.
Entweder fühlte er sich im Landheim nicht wohl oder er verfolgte ein
anderes, abenteuerlicheres Berufsziel als von den Eltern vorgesehen.
Jedenfalls verließ er nach einem guten halben Jahr, im August 1936, die
Schule und begann Ende 1936 im Alter von 15 Jahren eine Ausbildung an
der Marineschule in Civitavecchia mit dem Berufsziel Schiffsoffizier
der Handelsmarine.

Im August 1938 verließ er als 17-Jähriger auch diese Schule, da für ihn
dort kein regulärer Abschluss mehr möglich war.  Ihm gelang es aber, das
Diplom unter bestimmten Auflagen zu erhalten.  Von 1938 bis 1940 fuhr er
auf einem griechischen, später auf einem panamaianischen Schiff und
brachte es bis zum Navigationsoffizier.

Nachdem im April 1940 sein Schiff in britischen Gewässern torpediert
wurde, strandete er in England, konnte aber seine Laufbahn auf See ohne
die britische Staatsangehörigkeit nicht fortsetzen.  Er wurde in Kanada
interniert, kam am 30. Juni 1941 auf dem Schiff SS Indrapoera nach
England zurück und wurde am 6. Oktober 1941 freigelassen.  Franz trat in
das Pioneer Corps der britischen Armee ein. Später war er als Übersetzer
bei italienischen und deutschen Kriegsgefangenen in einem Lager in
Schottland tätig.

Während seine Schwester noch vor den Pogromen 1938 ebenfalls aus
Deutschland entkommen konnte und sich in den USA ansiedelte, hat der
NS-Apparat seine Eltern 1940 deportiert.  Sie überlebten die schlimmen
Zustände im Lager Gurs_ des Vichy_-Marionettenregimes nicht lang: Mutter
Flora starb noch 1940, Vater Saly anderthalb Jahre später.

In England heiratete Franz Oppenheimer am 7. November 1942 Renate
Ritter. Beide anglisierten am 11. Juni 1943 ihre Namen und hießen nun
Frank Stanley Orland und Catherine R. Orland.  Seine Schwester Eleonore
verwendete gelegentlich in Anlehnung daran den Namen Orland-Benedick als
Autorin in ihren Veröffentlichungen.

Im April 1945 brachte seine Frau die gemeinsame Tochter Patricia F.A.
Orland zur Welt.  1946 verließ Frank die Armee und lebte in London, wo
er als Vertreter arbeitete. Gesundheitlich ging es ihm nicht gut.

Am 16. Mai 1947 erhielt Frank Orland die britische Staatsbürgerschaft.
Mit seiner Schwester Eleonore stand er in Kontakt. Die Geschwister
führten einen jahrelangen und aufreibenden Kampf um Wiedergutmachung und
Entschädigung.

Im Juli 1968 heiratete die Tochter Patricia Anthony K. Sheppard in
Kensington, London und brachte im Dezember 1970 Franks Enkel Justin Mark
Sheppard zur Welt.

Frank Orland starb am 12. November 1972 im Alter von 51 Jahren in London.

.. _Saly Oppenheimer: oppenheimer-sa.html
.. _Flora: oppenheimer-fl.html
.. _Eleonore: sterling-el.html

`Mehr zu Franz Oppenheimer und seiner Famile <https://stolpersteine-heidelberg.de/mediapool/pdf/2024-fam-beer-oppenheimer.pdf>`_

.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Judenhaus: http://de.wikipedia.org/wiki/Judenhaus
.. _Maximilian Neu: neu-ma.html
.. _Franz: oppenheimer-fr.html
.. _Vichy: http://de.wikipedia.org/wiki/Vichy-Regime
