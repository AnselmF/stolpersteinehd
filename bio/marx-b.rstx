Bertha (Berthel) Marx
=====================

.. container:: dates

  -REPLACE-WITH-DATES-

Bertha (Berthel) Marx, wurde am 18. Juli 1870 in Bruchsal als Tochter
des Korsettfabrikanten Löw (Leopold) Gros (1833 - 1881) und seiner Frau
Rosa Gros, geb. Heymann (1847 - 1926) geboren. Sie heiratete 1891 in
Bruchsal Karl Marx (1863 - 1928). Sie brachte ihre drei Kinder Mally,
Ellen und Anne alle in Bruchsal zur Welt.

Berthel Marx war eine
außergewöhnlich schöne Frau, die sich dessen auch bewusst war. Sie
spielte gerne in geselliger Runde Bridge, liebte die Musik und war
selbst eine gute Klavierspielerin. Nach dem Tod ihres Mannes Karl Marx
lebte Berthel Marx seit Anfang der 1930er Jahre bei der Familie ihrer
ältesten Tochter `Amalie Liebhold`_ in Heidelberg.

Vor der ständig wachsenden Verfolgung – die deutschen Behörden hatten
ihren Schwiegersohn `Michael Liebhold`_ im Gefolge der Reichspogromnacht
ins KZ Dachau deportiert und dort im Effekt umgebracht – konnte sie
Anfang August 1939 in die Niederlande fliehen. Sie lebte dort in
Amsterdam, zunächst (im September und Oktober 1939 zusammen mit ihrer
ältesten Tochter) bei ihrer jüngeren Schwester Marie Schöndorff und
deren Mann Albert Schöndorff, die beide bereits 1938 in die Niederlande
geflohen waren, später dann in der Familie von Anna Maria Heiden Heimer,
geb. Deutsch (1902 - 1989), der Tochter ihres Cousins Otto (Nathan)
Deutsch (1867 - 1940).

Doch holte die NS-Verfolgung Berthel Marx ein, als die deutsche
Wehrmacht in den Niederlanden einfiel.  Die Deutschen sperrten sie
im Durchgangslager
Westerbork_ ein und von deportierten sie von dort am 20. Juli 1943 Richtung des
Vernichtungslagers Sobibor. Sie starb am 23. Juli 1943
während des Transports.  Auf dem jüdischen Friedhof in Bruchsal ist auf
dem Grabstein ihres Mannes Karl Marx auch ihr Name eingeschrieben.

Die die deutsche Besatzungsmacht ließ ihre ersten GastgeberInnen in den
Niederlanden, Marie und Albert Schöndorff, in Auschwitz ermorden.
Sie deportierte ihre späteren GastgeberInnen, die Familie Heiden Heimer, 
in das KZ Westerbork und dann nach Bergen-Belsen, wo sie
Hermann Heiden Heimer 1945 ermorden ließ.  Anna Maria Heiden Heimer
hingegen überlebte mit ihren Kindern wie durch ein Wunder das
NS-Lagersystem.

`Mehr zu Familie Liebhold <http://stolpersteine-heidelberg.de/familie-liebhold.html>`_

.. _Westerbork: http://de.wikipedia.org/wiki/Durchgangslager%20Westerbork

.. _Michael Liebhold: liebhold-m.html
.. _Amalie Liebhold: liebhold-a.html
.. _Ruth Liebhold: liebhold-r.html
.. _Martin Liebhold: liebhold-ma.html
.. _Klaus Liebhold: liebhold-k.html
.. _Berthel Marx: marx-b.html
.. _Käthe Zimmermann: zimmermann-k.html
