Dr. Ernst Max Sussmanowitz
==========================

.. container:: dates

  -REPLACE-WITH-DATES-

Ernst Sussmanowitz kam am 21. Februar 1909 als Sohn von Laura_ und `Isaak
Sussmanowitz`_ in Zeiskam (nahe Germersheim) zur Welt.  Er hatte eine
keine Schwester, Edith_.  Ihr Vater, der angesichts wachsenden
Antisemitismus' im zaristischen Russland ins deutsche Reich eingewandert
war, betrieb dort eine Arztpraxis.

1913 zog die Familie nach Speyer, wo Ernst seine Kindheit und Jugend
verbrachte.  Die Verhältnisse dort beschrieb seine Schwester `in einem
Gespräch im Jahr 2012`_; sie erinnerte sich schmerzhaft daran, wie sie
bereits als Kinder von Speyrer „Lausbuben“ als „schmutzige Juden“
beschimpft wurden und dass bei ihnen zu Hause, offenbar angesichts des
großen Drucks der Mehrheitsgesellschaft, wenig gelacht wurde.

.. _in einem Gespräch im Jahr 2012: https://www.speyer.de/de/familie-und-soziales/senioren/demografische-entwicklung/2012.2-interview-dr.-edith-szekely.pdf

Nach seinem Abitur begann Ernst ein Medizinstudium in Heidelberg. 1928
kam seine Schwester in die Stadt, 1929,
nachdem sein Vaters einen Herzinfarkt erlitten hatte, auch
seine Eltern. Die Familie wohnte für die
nächsten Jahre zunächst in der Zähringer Straße 8.  Beide Kinder
engagierten sich in verschiedenen linksradikalen Organisationen.

Als Sozialist und Jude hatte Ernst doppelt Grund, nach der Machtübergabe
an die NSDAP Deutschland zu verlassen.  Er ging er mit seiner Frau Irene
ins Exil nach Amsterdam, wo nach einer Weile auch seine Schwester
eintraf.  Er arbeitete später am Jüdischen Krankenhaus in Rotterdam.  Dort
erfuhr er, dass die jüdisch-amerikanische Hilfsorganisation Joint_ in
Russland eine Filiale eröffnet hatte, um dort Jüdinnen und Juden zu
helfen. Deutsche jüdische ÄrzIinnen waren gefragt. Mit seiner Frau, die
Krankenschwester war, brach er auf.  Er wurde auf der Krim eingesetzt
und berichtete enthusiastisch von dort.

.. _joint: https://de.wikipedia.org/wiki/Joint_Distribution_Committee

Doch schon Anfang 1937 verhafteten ihn die Sowjet-Behörden: Die
stalinistischen „Säuberungen“ hatten begonnen.

Am 1. November 1938 lässt der sowjetische Apparat Ernst Max Sussmanowitz
in Simferopol erschießen.

`Mehr zu Isaak Sussmanowitz und seiner Familie <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2014/2014_Familie_Sussmanowitz_Szekely.pdf>`_

.. _ernst: sussmanowitz-em.html
.. _edith: sussmanowitz-ed.html
.. _Laura: sussmanowitz-la.html
.. _Isaak Sussmanowitz: sussmanowitz-is.html
.. _Lajos Székely: szekely-la.html
.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
