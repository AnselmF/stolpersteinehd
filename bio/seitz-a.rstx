Alfred Seitz
=============

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: seitz-a.jpeg

Alfred Seitz wurde am 10. Februar 1903 in Mannheim geboren und stand der
SPD nahe. Er arbeitete nach der Ausbildung zum Krankenpfleger in
verschiedenen Krankenhäusern, schließlich als OP-Pfleger in der heutigen
Thoraxklinik in Heidelberg-Rohrbach.  Nach seiner Heirat mit `Käthe
Brunnemer`_ lebte das Paar zuletzt in der Karlsruher Straße 46 in der
Nähe seiner Arbeitsstelle.

Da sein Schwiegervater, Philipp Brunnemer, in regelmäßigem Austausch mit
`Georg Lechleiter`_ und anderen kommunistischen WiderstandskämpferInnen aus
Mannheim stand und seine Frau Käthe die Entwicklungen verfolgte, die zur
Gründung der Vorbote-Gruppe führten, bekam auch Alfred Seitz Kontakte zu
diesem Netzwerk.

.. _Georg Lechleiter: http://de.wikipedia.org/wiki/Georg%20Lechleiter

Im Jahr 1940 hatte ein Kreis um Georg Lechleiter vor allem in den
Mannheimer Großbetrieben mehrere antifaschistische Zellen aufgebaut, die
Spenden für die politischen Gefangenen und ihre Familien sammelten. Am
Tag des Überfalls auf die Sowjetunion, am 22. Juni 1941, beschlossen die
WiderstandskämpferInnen, ihre Aktivitäten zu verstärken. Bei einem
Treffen in der Wohnung von Käthe und Alfred Seitz wurde vereinbart, die
antifaschistische Zeitung „Der Vorbote“ herauszugeben.

An der Erstellung der illegalen Druckschrift, die nur an
vertrauenswürdige GenossInnen verteilt wurde, waren Dutzende von
AntifaschistInnen beteiligt.  Alfred Seitz unterstützte seine Frau bei
der Produktion der Zeitung. Unter anderem holte er vor der ersten
Ausgabe die Schreibmaschine bei einem konspirativen Treffpunkt ab und
las auch später die Matrizen vor der Vervielfältigung. Zudem hörte
Alfred Seitz zusammen mit seiner Frau verbotene Radiosender.

Im Herbst und Winter 1941 erschienen vier Ausgaben des „Vorboten“, die
weit über Mannheim hinaus Verbreitung fanden. In den Artikeln wurden
hauptsächlich die militärischen Entwicklungen mithilfe ausländischer
Nachrichten analysiert und die wirtschaftliche Situation dargestellt.
Einzelne Beiträge beschäftigten sich auch mit konspirativem Verhalten
und organisatorischen Überlegungen zum Widerstand.

Anfang 1942 kam die Gestapo der Gruppe auf die Spur. Am 26. Februar
1942, während der Produktion der 5. Ausgabe, setzten die
Massenverhaftungen ein, und am 1. März wurde Alfred Seitz festgenommen.
Schon am 14./15. Mai 1942 fand vor dem berüchtigten Volksgerichtshof ein
erster Prozess gegen 14 AntifaschistInnen statt, darunter Alfred Seitz,
seine Frau und sein Schwiegervater Philipp Brunnemer. Käthe Seitz
versuchte, ihren Mann zu entlasten, der eine weniger zentrale Rolle
gespielt hatte. „Aber der sonst so stille Pfleger erklärte mit fester
Stimme, er habe sein ganzes Leben mit seiner Ehefrau geteilt und wolle
nun auch mit ihr in den Tod gehen“ (Oppenheimer, Der Fall Vorbote, S.
96), wie sich ProzessbeobachterInnen erinnerten. Die Richter
– „Vizepräsident des Volksgerichtshofs Engert, Vorsitzender;
Kammergerichtsrat Discher; SA-Gruppenführer Petersen;
Generalarbeitsführer Voigt; SS-Brigadeführer Herm“ (ebd) – verurteilten
alle 14 Angeklagten zum Tod.

Nach einem halben Jahr in der Todeszelle wurde der 15. September für
alle 14 Verurteilten als Hinrichtungstermin angesetzt. Über die letzte
Nacht schrieb der Anstaltsgeistliche später: „Die Eltern der Hilde
Janssen (Käthe Seitz‘ Tochter aus erster Ehe), Alfred und Käthe
Seitz, wurden damals hingerichtet, ebenso der Großvater. (…) Alle drei
waren Dissidenten und wünschten keinen geistlichen Beistand. Dagegen
unterhielten wir uns angeregt. […] Der Vater war Krankenpfleger
in Heidelberg und trug noch Grüße auf an die dortigen
Schwestern“ (zit. nach Oppenheimer S. 110f).


In den frühen Morgenstunden des 15. September 1942 haben Beschäftigte
der Stuttgarter Justiz Alfred Seitz
zusammen mit seiner Frau Käthe, seinem Schwiegervater und den übrigen 11
GenossInnen hingerichtet.

Die Leichen wurden danach nicht zur Bestattung freigegeben, sondern an
die Anatomie der Universität Heidelberg für Sezierübungen übergeben.
Auch nach der Befreiung war ein mehrjähriger Kampf nötig, bis die
Universität sie den überlebenden Angehörigen aushändigte. Seit 1950
ruhen die sterblichen Überreste von Alfred Seitz und seinen
MitstreiterInnen an der `Gedenkstätte auf dem Heidelberger
Bergfriedhof`_.

.. _Gedenkstätte auf dem Heidelberger Bergfriedhof: https://www.openstreetmap.org/#map=19/49.39465/8.69163

`Mehr zu Käthe und Alfred Seitz
<http://stolpersteine-heidelberg.de/kaethe-alfred-seitz.html>`_

.. _Käthe Brunnemer: seitz-k.html
