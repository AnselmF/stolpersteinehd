Karl Wertheimer
===============

.. container:: dates

  -REPLACE-WITH-DATES-

Karl Wertheimer kam am 2. Februar 1915 in Neustadt als erstes Kind des
Schneiders `Julius Wertheimer`_ und seiner Frau Klara_ zur Welt.  Die
Familie zog vor 1920 nach Heidelberg, wo Klara 1924 Karls kleinen Bruder
Fritz_ zur Welt brachte.

Als Kind war er Mitglied des jüdischen Wanderbundes „Die Kameraden“.

Nach der nach der Machtübergabe an die Nationalsozialisten veranlassten
Liquidation des elterlichen Geschäfts zog die Familie 1935 in die
Hauptstraße 187.  Die Gestapo verhörte Karl mehrfach.  Am 16. Oktober
1937 gelang ihm schließlich
die Flucht aus Deutschland nach Kolumbien.

Dort heiratete er, wurde Vater von zwei Kindern, und war als Kaufmann
tätig.  1961 lebte er in Bogota.

Er war der einzige Überlebende der Familie.  Der deutsche Apparat ließ
die in Heidelberg verbliebenen Familienangehörigen zunächst im
Internierungslager Gurs_ einsperren, wo seine Eltern schnell den
furchtbaren Bedingungen zum Opfer fielen.  Sein kleiner Bruder überlebte
zunächst, doch haben die Behörden auch ihn schließlich im
Vernichtungslager Auschwitz ermorden lassen.

`Mehr zur Familie Wertheimer <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2015/2015_Familie_Wertheimer.pdf>`_

.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _julius wertheimer: wertheimer-j.html
.. _fritz: wertheimer-fs.html
.. _klara: wertheimer-k.html
