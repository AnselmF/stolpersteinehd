Gretel Linick
=============

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: linick-e+g.jpeg

   Gretel Linick mit ihrem Bruder Edgar.

Gretel Linick wurde am 30. September 1906 als zweites Kind von `David
Linick`_ und dessen Frau Bertha_ in Heidelberg geboren und
wuchs zusammen mit ihrem älteren Bruder Edgar_ in der Altstadt auf. Ihre
Eltern führten in der Hauptstraße 120 die Textilwarenhandlung
Dührenheimer und Ledermann, die 1920 ihren Sitz in die Plöck 36
verlegte. Die Familie zog mehrfach um und wohnte schließlich ab Anfang
der 1930er Jahre im Haus Neckarstaden 20, das sich ebenso wie die Plöck
36 in ihrem Besitz befand.

Nach dem Besuch der Mädchen-Oberschule und der mittleren Reife ließ
Gretel Linick sich zur Schneiderin ausbilden und legte die
Meisterprüfung ab. Am 10. Februar 1932 meldete sie ein eigenes
„kunstgewerbliches Atelier für Kinderkleidung“ im Neckarstaden 20 an.

Die Machtübertragung an die Nazis traf die Familie schwer, nicht nur
durch die Boykottaktionen gegen die Textilhandlung in der Plöck. Edgar
Linick, der sich in der Weimarer Zeit sozialistischen Kreisen
angeschlossen hatte, sah sich als Jude und Kommunist doppelt gefährdet
und ging im September 1933 ins Exil nach Spanien.

Auch Gretel Linicks
Verdienstmöglichkeiten waren durch die Boykottaktionen zunehmend
eingeschränkt. Im Sommer 1936 verließ sie die Stadt, reiste illegal in
die Schweiz aus und folgte ihrem Bruder nach Spanien, wo sich beide auf
der Seite der Republik gegen den faschistischen Franco-Putsch
einsetzten.

Während sich ihr Bruder den Internationalen Brigaden anschloss und im
Sanitätsdienst tätig war, arbeitete Gretel Linick in Barcelona als
Schneiderin und engagierte sich in antifaschistischen Exilkreisen.
Angaben zu ihrer Tätigkeit als Krankenschwester in Lazaretten der
Internationalen Brigaden konnten nicht bestätigt werden. Von Barcelona
aus hielt sie engen Briefkontakt zu Edgar und vielen weiteren
GenossInnen sowie zu ihren Eltern in Heidelberg, deren Auswanderung nach
Spanien sie zu ermöglichen versuchte.

Ein schwerer Schlag für Gretel Linick war die Verhaftung ihres Partners,
des ungarischen Kommunisten Karoly Karoly, der sich Anfang 1937 mit der
Parteispitze überworfen hatte und aus den Kommunistischen Parteien
Belgiens und Ungarns ausgeschlossen worden war. Nachdem er bei der
Verhaftungswelle gegen AnhängerInnen der sozialistischen POUM und
anderer oppositioneller Gruppen festgenommen worden war, trug sich die
junge Frau kurzzeitig mit dem Gedanken, Spanien zu verlassen, entschloss
sich dann aber doch zu bleiben.

Als Ende 1938 der Sieg der faschistischen Übermacht greifbar war und die
internationalen UnterstützerInnen zur Ausreise aufgefordert wurden,
reiste Gretel Linick nach Frankreich aus. Dort wurde sie gemeinsam mit
den anderen AntifaschistInnen und der geflüchteten Zivilbevölkerung von
den Behörden interniert und durchlebte eine Odyssee durch die
französischen Lager: Nach mehreren Monaten in St. Zacharie wurde die
Heidelbergerin ins Camp de Hyères und schließlich nach Gurs_ verbracht.

.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs

1940 wurde Gretel Linick wie viele der internierten Frauen freigelassen
und lebte mit einer Gruppe weiterer deutschsprachiger
Interbrigadistinnen im Dorf Vidauban (Dep. Var), wo sie Unterstützung
vom „Hilfskomitee für die internierten deutschen und österreichischen
Spanienkämpfer“ erhielten. Durch Briefe hielt sie weiterhin enge
Kontakte zu vielen AntifaschistInnen und vermutlich auch zu ihren
Eltern, die nach ihrer Abschiebung aus Baden im Oktober 1940 in Gurs
internert waren.

Im Jahr 1942 stieg der Verfolgungsdruck erneut an, als die Deutschen von den
willig kooperierenden Vichy_-Behörden einforderten, die in
Südfrankreich teils internierten, teils noch auf freiem Fuß lebenden
jüdischen Menschen auszuliefern. Die
Verhaftungswellen häuften sich, und am 12. September 1942 wurde auch Gretel
Linick verhaftet. Über Rivesaltes wurde sie in das Sammellager Drancy
bei Paris verschleppt.  Von dort deportierte der deutsche Apparat sie am
16. September 1942 mit einem Großtransport nach Auschwitz und ließ sie
dort ermorden.

`Mehr zur Familie Linick <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2020/71_Linick_Text.pdf>`_

.. _David Linick: linick-d.html
.. _Bertha: linick-b.html
.. _Edgar: linick-e.html
.. _Gretel Linick: linick-g.html
.. _Vichy: http://de.wikipedia.org/wiki/Vichy-Regime
