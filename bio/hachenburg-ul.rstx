Ulrich Hachenburg, später Roger William Harrison
================================================

.. container:: dates

  -REPLACE-WITH-DATES-

Ulrich Hachenburg wurde am 15. Januar 1926 im Landkreis Dachau geboren
und 1927 von Hans_ und `Hildegard Hachenburg`_ adoptiert.  Diese – ein
Anwalt mit Kanzlei in Mannheim und eine evangelische Theologin – lebten
damals bereits seit zwei Jahren in Heidelberg.  1932 zog die Familie in
die Kuno-Fischer-Straße 4 in Neuenheim. Ulrich besuchte die
Mönchhofschule.

Ulrichs Vater war zwar zum protestantischen Glauben seiner Mutter
konvertiert, galt aber im NS-Staat weiter als Jude und wurde
entsprechend verfolgt.  Ulrich verließ Heidelberg daher im Jahr 1936 und
besuchte anschließend das Internat Salem am Bodensee, „um mich nicht
Anfeindungen auszusetzen von  denen wir wußten, daß sie mich im
Heidelberger Gymnasium erwartet hätten,“ wie er in einer
Gedächtnisvorlesung an der Uni Heidelberg in den 1990er Jahren
formulierte.

Auf diese Weise blieb ihm erspart, Zeuge der Novemberpogrome_ 1938 zu
werden, während derer MitbürgerInnen und SS-Trupps die Wohnung seiner
Eltern zerstörten.  Tatsächlich war er zu dem Zeitpunkt bereits in
relativer Sicherheit, da ihn ein weitsichtiger Internatslehrer nach
einer  Scharlacherkrankung „zur Kur“ in die Schweiz geschickt hatte.

Nachdem die Polizei seinen Vater nach der Pogromnacht für einen Monat in
das `Konzentrationslager Dachau`_ verschleppt hatte, flohen seine Eltern
im März 1939 nach England.  Ulrich konnte im Sommer 1939 folgen.
Dank der Unterstützung von Anny  Ostwald, einer Wiener Bankierswitwe und
Verehrerin von Ulrichs Großvater Max_, konnte die  Familie ein kleines
Häuschen in Oswestry_ südlich von Liverpool mieten.

Nach der Kapitulation Frankreichs wurde das Ehepaar Hachenburg als
angebliche deutsche Spione verhaftet und für eine Weile auf der Isle of
Man interniert.  Der nun wieder elternlose  Sohn Ulrich besuchte die
Oxford Country Community School.  Seine dortige  Pflegemutter gab ihm
den neuen Namen  „Roger William Harrison“, den sie willkürlich dem
örtlichen Telefonbuch entnommen hatte.  Unter diesem Namen legte er 1944
sein Abitur ab.

Er leiste dann seinen Militärdienst, besuchte eine Offizierschule, war
im italienischen Triest eingesetzt.  1948 verabschiedete ihn die Royal
Army.

Im Jahr 2000 berichtete Roger W. Harrison in einem Vortrag über das
weitere Schicksal seiner Eltern, die schon  bald nach Kriegsende die
Rückkehr nach  Deutschland planten. Für seinen Großvater – der 1939 über
die Schweiz ebenfalls nach England hatte fliehen können – kam zwar eine
Heimkehr nicht in Frage, nachdem seine Frau bereits 1933, „ein halbes
Jahr nach Beginn der Willkürherrschaft [gestorben war] an einem Leiden
zu dem  die Aufregung der ersten Monate
[der NS-Herrschaft] ohne  Zweifel beigetragen hatte“
(Harrisons Worte) und der NS-Apparat später seine beiden Töchter hatte
umbringen lassen.

Hans und Hildegard Hachenburg hingegen kehrten rasch wieder nach
Deutschland zurück; Hans arbeitete zunächst bei den `Nürnberger
Prozessen`_ mit und war später Landgerichtsdirektor in Heidelberg.

Roger W. Harrison wiederum arbeitete als Kaufmann in Malaysia, Singapur
und Indonesien.  Er besuchte seine Eltern in größeren Abständen.  Bei
einer dieser Gelegenheiten verliebte er sich in die Tochter der engsten
Freundin seiner Mutter, Elisabeth Noeldchen.  1956 heirateten sie, das
Paar zog nach London. Sie bekamen zwei Kinder, Rupert und Marol.
Harrison arbeitete bis 1972 als Rohstoffmakler.

Er hatte aber zeitlebens eine stille Sehnsucht nach Heidelberg behalten.
Im Jahr 1975, nach dem Unfalltod seiner Eltern, die bis dahin in
Heidelberg gelebt hatten, entschied sich das Ehepaar Harrison, nach
Neckargemünd zu ziehen.  Roger arbeitete weiter im Rohstoffhandel, nun
aber von Frankfurt aus.

Roger W.  Harrison starb am 1. Juni 2008 in Heidelberg.

.. _Oswestry: https://en.wikipedia.org/wiki/Oswestry
.. _Konzentrationslager Dachau: http://de.wikipedia.org/wiki/KZ%20Dachau

`Mehr zu Familie Hachenburg
<http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2022/01_Hachenburg_Stolpersteine_2022_Finale_Datei-2.pdf>`_

.. _Nürnberger Prozessen: https://de.wikipedia.org/wiki/N%C3%BCrnberger_Prozesse
.. _Novemberpogrome: http://de.wikipedia.org/wiki/Novemberpogrom%201938
.. _Judenhaus: http://de.wikipedia.org/wiki/Judenhaus
.. _Konzentrationslager Dachau: http://de.wikipedia.org/wiki/KZ%20Dachau
.. _Leontine Goldschmidt: goldschmidt-le.html
.. _Theresienstadt: http://de.wikipedia.org/wiki/KZ%20Theresienstadt
.. _Lucie Gudula Simons: hachenburg-lu.html
.. _Hildegard Hachenburg: hachenburg-hi.html
.. _Liese Hachenburg: hachenburg-el.html
.. _Hans: hachenburg-ha.html
.. _Max: hachenburg-ma.html
.. _Ulrich Hachenburg: hachenburg-ul.html
