Flora Oppenheimer, geb. Beer
============================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: beer-fl.jpeg

   Flora Oppenheimer vor dem ersten Weltkrieg im Tennisclub Sinsheim
   (Foto: Stadtarchiv Sinsheim)

Flora Beer wurde am 17. Mai 1895 als Tochter von Anna und `Julius Beer`_
in Sinsheim geboren.  Sie wuchs in einem Unternehmerhaushalt auf, ihr
Vater betrieb eine Hadernsortieranstalt, also einen Zulieferer der
Papierindustrie.

Sie blieb das einzige Kind ihrer Eltern und wuchs im stattlichen Haus an
der Muthstraße auf. Zunächst besuchte sie die allgemeine Volksschule –
1876 waren im Großherzogtum Baden die Konfessionsschulen abgeschafft und
die Simultanschule mit getrenntem Religionsunterricht die Regel. Später
wechselte Flora auf die Sinsheimer Realschule, die sie aber nur bis zur
Obertertia besuchte – wir wissen nicht, warum sie die Schule abbrach.

Auf ihrer späteren Kennkarte ist vermerkt, dass sie ein Diplom in
Säuglings- und Krankenpflege erworben hat und während des 1. Weltkriegs
Mitglied beim Roten Kreuz war. Ob sie ihren gelernten Beruf ausübte oder
während des 1. Weltkriegs in einem Lazarett arbeitete, ist nicht mehr
festzustellen. Viele deutsche Frauen – nichtjüdische und jüdische –
waren damals dem „Nationalen Frauendienst“ beigetreten, in der Hoffnung
auf Gleichberechtigung in Friedenszeiten. Wenigstens das allgemeine
Wahlrecht haben sie 1919 erkämpfen können.

.. _Julius Beer: beer-ju.html

Im Jahr 1920 heiratete Flora Beer den Dossenheimer Kaufmann `Saly
Oppenheimer`_.  Das Paar gründete seinen Hausstand weder in Dossenheim
noch in Sinsheim, sondern in der Stadt Heidelberg.
Sie wohnten zunächst in Neuenheim und zogen im Frühjahr 1921 in die
Weststadt, zuerst in die Zähringerstraße 25, dann 1925 in die
Zähringerstraße 3a. Die Weststadt war nicht nur „ein gutbürgerlicher
Stadtteil“ (Seemann), hier wohnten auch die meisten HeidelbergerInnen
jüdischen Glaubens, nämlich 346 Personen, das heißt fast ein Drittel der
WeststädterInnen.

.. _Saly Oppenheimer: oppenheimer-sa.html

In der Zähringerstraße 15 lag die „Privatklinik für Geburtshilfe und
Frauenkrankheiten“ von Professor `Maximilian Neu`_, in der Flora
Oppenheimer die gemeinsamen Kinder Franz_ und Eleonore_ zur Welt brachte.
Gegenüber ihrer Wohnung in der Zähringerstraße 6 lebte die Familie
Morgenthal. Später im englischen Exil erinnerte sich der Reformpädagoge
und Pazifist Moritz Morgenthal des „regelmäßigen Kontakts“ mit Flora und
Saly Oppenheimer. In der Nachbarschaft wohnten auch Schwager Leopold
Oppenheimer und dessen Frau Rositta mit ihren beiden Söhnen Hans und
Max.

Die Pestalozzischule (Volksschule), die Franz und Ellie besuchten, war
nur einige Schritte entfernt.  In dieser anregenden Umgebung führte Flora
ein gastfreundliches Haus, wie eine Freundin des Hauses später schrieb.
Wir wissen nicht, ob und wo sich Flora Oppenheimer engagierte, da ihr
und ihrer Kinder persönlicher Nachlass verloren ist.  Sicher waren sie
und ihr Mann liebevolle Eltern – ihre Tochter Eleonore betonte später
die Bedeutung einer liebevollen Erziehung: „Es ist daher wichtig, daß
wir unsere Jugend mit Milde und Wärme erziehen und sie zu echter
Erkenntnis vorbereiten. Sie soll die Kraft und den Mut besitzen ihre
Umgebung zu durchschauen,“ heißt es in einem ihrer Artikel für die
Gewerkschaftlichen Monatshefte aus dem Jahr 1963.

1926 trat ihr Mann ins Unternehmen ihres Vaters ein und wurde 1930 dort
Geschäftsführer.

Mit der Machtübergabe an die NSDAP 1933 waren für Flora und Saly die
schönen Jahre vorbei.  Sie hatten für ihre Kinder den Besuch einer
höheren Schule und ein Studium vorgesehen. Angesichts der Schikanen,
denen ihre Kinder bereits 1933 in der Schule ausgesetzt waren, schätzten
sie sehr realistisch ein, dass sich diese Pläne in Heidelberg nicht mehr
verwirklichen ließen und meldeten den Sohn Franz im Alter von 14 Jahren
in Heidelberg ab. Sie schickten ihn in ein Landschulheim in Florenz.

Die schöne große Wohnung in der Zähringerstraße war wohl nicht mehr zu
halten, die nun dreiköpfige Familie zog am 1. Juli 1936 in die nahe
Bunsenstraße 19a in eine kleinere Wohnung um.  Nachdem Floras Mann das
Sinsheimer Unternehmen im Zuge der „Arisierung“ hatte notverkaufen
müssen, zog auch ihr Vater Julius Beer mit in die Bunsenstraße.

Am 6. September 1938 trennten sich Flora und Saly auch von ihrer Tochter.
Sie schickten die 13-Jährige allein mit dem Schiff nach New York zu
einem Cousin von Flora, Walter Benedick. Er hatte alle Verpflichtungen
für die Einreise und den Aufenthalt für Eleonore in den USA übernommen.

Wenige Wochen nach der Pogromnacht_ starb im November 1938 Floras Vater.

.. _Pogromnacht: http://de.wikipedia.org/wiki/Novemberpogrom%201938

Das Ehepaar plante nun ebenfalls seine Ausreise in die USA. Am 9. Dezember
1939 gab Cousin Walter Benedick eine eidesstattliche Erklärung ab, damit
auch Saly und Flora ein Visum erhalten konnten.  Die Flucht scheiterte
aber, weil Saly für die US-Behörden als behindert galt.

1940 musste das Ehepaar in das „Judenhaus_“ in der Bluntschlistraße 4
umziehen.  Dort haben Polizisten sie zur `Wagner-Bürckel-Aktion`_
abgeholt, der ersten großen Massendeportation deutscher JüdInnen.  Im
Zug ins Lager Gurs_ saßen auch Floras Schwager Sigmund und Leopold mit
deren Ehefrauen Clara und Rositta sowie ihre Schwägerin Rosa und ihr
Neffe Hans.

Die Vichy_-Behörden haben Flora im Lager von ihrem Mann getrennt.  Sie
erkrankte bald nach der Ankunft und starb am 22.  Dezember 1940 im Alter
von von 45 an Entkräftung, Enteritis und letztlich auch gebrochenem
Lebenswillen.

`Mehr zu Flora Oppenheimer und ihrer Famile <https://stolpersteine-heidelberg.de/mediapool/pdf/2024-fam-beer-oppenheimer.pdf>`_

.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Judenhaus: http://de.wikipedia.org/wiki/Judenhaus
.. _Maximilian Neu: neu-ma.html
.. _Franz: oppenheimer-fr.html
.. _Eleonore: oppenheimer-el.html
.. _Vichy: http://de.wikipedia.org/wiki/Vichy-Regime
